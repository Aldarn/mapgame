import os
import json
from DomainObject import DomainObject

class Map(DomainObject):
    EMPTY_TILE_ID = 0

    @classmethod
    def fromSize(cls, game, size):
        mapData = {"id": -1, "name": None}

        tileData = []
        for y in range(size):
            tileData.append([])
            for x in range(size):
                tileData[y].append(Map.EMPTY_TILE_ID)

        return Map(game, mapData, tileData, {}, {})

    def __init__(self, game, mapData, tileData, npcData, actionData):
        super(Map, self).__init__(game)

        # Set the fields
        self.id = mapData["id"]
        self.name = mapData["name"]
        self.localFilePath = None

        # Load the NPCs
        # TODO properly
        self.npcData = npcData

        # Load the actions
        # TODO properly
        self.actionData = actionData

        # Load the tiles
        self.map = {}
        for y in range(len(tileData)):
            self.map[y] = {}
            for x in range(len(tileData[y])):
                self.map[y][x] = self.game.TILE_TYPES[tileData[y][x]].createTile(self, x, y)

    def getTile(self, x, y):
        try:
            return self.map[y][x]
        except Exception, e:
            pass
        return None

    def setTile(self, x, y, newTile):
        if y not in self.map:
            self.map[y] = {}
        self.map[y][x] = newTile

    def resize(self, width, height):
        # Trim the y axis if it needs
        if len(self.map) > height:
            for y in range(len(self.map)):
                if y >= height:
                    del self.map[y]

        # Trim the x axis if it needs
        if len(self.map[0]) > width:
            for y in range(len(self.map)):
                for x in range(len(self.map[y])):
                    if x >= width:
                        del self.map[y][x]

        for y in range(height):
            if y not in self.map:
                self.map[y] = {}
            for x in range(width):
                if x not in self.map[y]:
                    self.map[y][x] = self.game.TILE_TYPES[Map.EMPTY_TILE_ID].createTile(self, x, y)

    def uploadToServer(self):
        # Get an id if it doesn't have one
        if self.id == -1:
            newId = self.game.client.API.getNewMapId()
            self.id = newId

        self.game.client.API.saveMap(self, self.getMapData())

    def getMapData(self):
        return {
            "mapData": {
                "id": self.id,
                "name": self.name
            },
            "tileData": [[tile.tileType.id for tile in self.map[y].itervalues()] for y in self.map.iterkeys()],
            "npcData": self.npcData,
            "actionData": self.actionData
        }

    def saveLocally(self, filePath = None):
        if not filePath:
            filePath = self.localFilePath
        else:
            self.localFilePath = filePath

        with open(filePath, 'w') as mapFile:
            os.chmod(filePath, 0777)
            mapFile.write(json.dumps(self.getMapData()))
