class NPC(object):
    def __init__(self, id, npcType, attackableNPC, map, x, y):
        self.id = id
        self.type = npcType
        self.attackableNPC = attackableNPC
        self.map = map
        self.x = x
        self.y = y