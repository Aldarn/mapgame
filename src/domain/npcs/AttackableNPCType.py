from NPCType import NPCType
from AttackableNPC import AttackableNPC

class AttackableNPCType (object):
    def __init__(self, id, respawnDelay, health, damage, defence, accuracy, agility, critical, block, poison, lifeSteal):
        self.id = id
        self.respawnDelay = respawnDelay
        self.health = health
        self.damage = damage
        self.defence = defence
        self.accuracy = accuracy
        self.agility = agility
        self.critical = critical
        self.block = block
        self.poison = poison
        self.lifeSteal = lifeSteal
