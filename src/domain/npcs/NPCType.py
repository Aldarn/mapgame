import config

class NPCType (object):
    def __init__(self, id, name, walkable, rectWidth, rectHeight):
        self.id = id
        self.name = name
        self.walkable = walkable;
        self.rectWidth = rectWidth;
        self.rectHeight = rectHeight;