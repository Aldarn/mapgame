from NPC import NPC

class AttackableNPC(object):
    def __init__(self, id, attackableNPCType, health, respawnTime):
        self.id = id
        self.attackableNPCType = attackableNPCType
        self.health = health
        self.respawnTime = respawnTime
