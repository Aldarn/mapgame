

"""
Generic glass representing an object pool.
"""
class ObjectPool (object):

	OBJECTS = {}

	def __init__(self, objType):
		self.objType = objType

	def get(self, id):
		try:
			return self.OBJECTS[id]
		except Exception, e:
			obj = self.objType.get(id)
			self.OBJECTS[id] = obj
			return obj
