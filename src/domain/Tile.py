from DomainObject import DomainObject

class Tile(DomainObject):
    def __init__(self, game, tileType, map, x, y):
        super(Tile, self).__init__(game)
        self.tileType = tileType
        self.map = map
        self.x = x
        self.y = y