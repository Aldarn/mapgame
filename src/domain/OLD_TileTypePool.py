from OLD_ObjectPool import ObjectPool
from TileType import TileType

class TileTypePool (ObjectPool):
	def __init__(self):
		super(TileTypePool, self).__init__(TileType.__class__)

		for tileType in TileType.getAll():
			self.OBJECTS[tileType.id] = tileType


