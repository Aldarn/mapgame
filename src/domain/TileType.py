from PyQt5 import QtGui
from DomainObject import DomainObject
from Tile import Tile

import os
from server.mapgame import config

class TileType(DomainObject):
    def __init__(self, game, id, name, imagePath):
        super(TileType, self).__init__(game)

        self.id = id
        self.name = name
        self.imagePath = imagePath
        self.image = None

    def getImage(self):
        if self.image is None:
            self.image = QtGui.QPixmap(os.path.join(config.EDITOR_TILE_IMAGE_PATH, self.imagePath))
        return self.image

    def createTile(self, map, x, y):
        return Tile(self.game, self, map, x, y)
