from PyQt5 import QtCore, QtGui, QtWidgets

class ClickableLabel(QtWidgets.QLabel):

    clicked = QtCore.pyqtSignal(object, object)
    hovered = QtCore.pyqtSignal(object, object)

    def __init(self, parent):
        QtWidgets.QLabel.__init__(self, parent)

    def mousePressEvent(self, event):
        self.clicked.emit(self, event)

    def mouseMoveEvent(self, event):
        hoveredWidget = QtWidgets.QApplication.widgetAt(event.globalPos())
        if hoveredWidget and isinstance(hoveredWidget, ClickableLabel) and hoveredWidget.parent() == self.parent():
            self.hovered.emit(hoveredWidget, event)
