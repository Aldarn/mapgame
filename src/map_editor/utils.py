def floodFill(map, x, y, oldTileType, newTileType):
	if not inBoundaries(x, y):
		return

	if getTile(x, y).getType() == oldFileType:
		setTile(newTileType.createTile(x, y))

	floodFill(map, x + 1, y, oldTileType, newTileType)
	floodFill(map, x - 1, y, oldTileType, newTileType)
	floodFill(map, x, y + 1, oldTileType, newTileType)
	floodFill(map, x, y-1, oldTileType, newTileType)



