import sys
sys.path.append("../")

from PyQt5 import QtCore, QtGui, QtWidgets

from game.Game import Game
from MapEditorWindow import MapEditorWindow
from rest_client_api.ClientAPI import ClientAPI


class MapEditor(object):
    GAME = None
    GUI = None
    API = None
    QAPP = None

    def __init__(self, argv):
        # Init client API
        MapEditor.API = ClientAPI(self)

        # Init game & data
        MapEditor.GAME = Game(self)

        # Launch GUI
        MapEditor.QAPP = QtWidgets.QApplication(argv)
        MapEditor.GUI = MapEditorWindow(self)
        MapEditor.GUI.show()
        MapEditor.QAPP.exec_()


if __name__ == "__main__":
    MapEditor(sys.argv)