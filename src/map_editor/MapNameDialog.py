import os

from PyQt5 import QtCore, QtGui, QtWidgets
from ui.map_name_dialog import Ui_MapNameDialog

class MapNameDialog(QtWidgets.QDialog, Ui_MapNameDialog):
    def __init__(self, mapEditor, currentName, parent = None):
        self.mapEditor = mapEditor
        self.mapName = currentName

        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.mapNameBox.setText(currentName)

        self.saveButton.clicked.connect(self.save)
        self.cancelButton.clicked.connect(self.reject)

    def save(self):
        if len(self.mapNameBox.text()):
            self.mapName = self.mapNameBox.text()
            self.accept()

    def getMapName(self):
        return self.mapName
