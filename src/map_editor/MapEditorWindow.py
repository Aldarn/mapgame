import os
import json

from PyQt5 import QtCore, QtGui, QtWidgets
from ui.map_editor import Ui_MapEditor
from LoadDialog import LoadDialog
from NewTileTypeDialog import NewTileTypeDialog
from MapNameDialog import MapNameDialog
from widget.ClickableLabel import ClickableLabel
from domain.Map import Map

class MapEditorWindow(QtWidgets.QMainWindow, Ui_MapEditor):
    # Map size constants
    DEFAULT_MAP_SIZE = 10
    DEFAULT_TILE_SIZE = 55
    MAX_TILE_SIZE = 110
    MAX_MINI_MAP_TILE_SIZE = 10
    DEFAULT_ZOOM_INCREMENT = 15
    CURRENT_TILE_SIZE = DEFAULT_TILE_SIZE

    def __init__(self, mapEditor, parent=None):
        self.mapEditor = mapEditor
        self.map = None

        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        # Setup menu events
        self.setupMenu()

        # Setup other events
        self.setupEvents()

        # Remove template tiles
        self.removeTemplateTiles()

        # Create the new map
        self.newMap()

        # Setup the palette
        self.createPalette()

    def setupMenu(self):
        # Save and commit only possible when something is loaded
        self.actionSave.setEnabled(False)
        self.actionCommit.setEnabled(False)

        self.actionNew.triggered.connect(self.newMapClicked)
        self.actionOpen.triggered.connect(self.openClicked)
        self.actionLoad.triggered.connect(self.menuLoadClicked)
        self.actionNewTileType.triggered.connect(self.newTileTypeClicked)
        self.actionCommit.triggered.connect(self.commitClicked)
        self.actionSave.triggered.connect(self.saveClicked)
        self.actionCommitAs.triggered.connect(self.commitAsClicked)
        self.actionSaveAs.triggered.connect(self.saveAsClicked)

    def openClicked(self):
        filePath = QtWidgets.QFileDialog.getOpenFileName(self, "Select Map", "../server/mapgame/data/maps/")[0]
        with open(filePath, 'r') as mapFile:
            mapJson = mapFile.read()

        # Decode the map json file
        data = json.loads(mapJson)

        self.map = Map(self.mapEditor.GAME, data["mapData"], data["tileData"], data["npcData"], data["actionData"])
        self.load()

    def newMapClicked(self):
        self.newMap()

    def newMap(self):
        # No committing until a name is chosen
        self.actionCommit.setEnabled(False)

        # Create the map
        self.map = Map.fromSize(self.mapEditor.GAME, MapEditorWindow.DEFAULT_MAP_SIZE)

        # Set the size
        self.mapSizeWidthBox.setValue(MapEditorWindow.DEFAULT_MAP_SIZE)
        self.mapSizeHeightBox.setValue(MapEditorWindow.DEFAULT_MAP_SIZE)

        # Setup the tiles
        self.drawTiles(setup = True)

    def commitClicked(self):
        self.map.uploadToServer()

    def commitAsClicked(self):
        mapNameDialog = MapNameDialog(self.mapEditor, self.map.name, self)
        if mapNameDialog.exec_():
            mapName = mapNameDialog.getMapName()
            if mapName:
                self.map.name = mapName
                self.map.uploadToServer()
                self.actionCommit.setEnabled(True)

    def saveClicked(self):
        self.map.saveLocally()

    def saveAsClicked(self):
        if self.map.localFilePath:
            targetPath = self.map.localFilePath
        else:
            targetPath = "../server/mapgame/data/maps/map_name.json"

        filePath = QtWidgets.QFileDialog.getSaveFileName(self, "Save Map As...",
            targetPath,
            "Json (*.json)"
        )

        self.map.saveLocally(filePath[0])
        self.actionSave.setEnabled(True)

    def setupEvents(self):
        self.setMapSizeButton.clicked.connect(self.setMapSizeClicked)
        self.newPaletteTile.clicked.connect(self.newTileTypeClicked)
        self.zoomInTool.clicked.connect(self.zoomInClicked)
        self.zoomOutTool.clicked.connect(self.zoomOutClicked)

    def setMapSizeClicked(self):
        newWidth = int(self.mapSizeWidthBox.text())
        newHeight = int(self.mapSizeHeightBox.text())

        if newWidth != len(self.map.map) or newHeight != len(self.map.map[0]):
            self.map.resize(newWidth, newHeight)
            self.drawTiles(setup = True)

    def zoomInClicked(self):
        newTileSize = MapEditorWindow.CURRENT_TILE_SIZE + MapEditorWindow.DEFAULT_ZOOM_INCREMENT
        if newTileSize < MapEditorWindow.MAX_TILE_SIZE:
            MapEditorWindow.CURRENT_TILE_SIZE = newTileSize
            self.resizeMapTiles()

    def zoomOutClicked(self):
        newTileSize = MapEditorWindow.CURRENT_TILE_SIZE - MapEditorWindow.DEFAULT_ZOOM_INCREMENT
        if newTileSize > 0:
            MapEditorWindow.CURRENT_TILE_SIZE = newTileSize
            self.resizeMapTiles()

    def removeTemplateTiles(self):
        templateTileWidget = self.currentMapGrid().itemAtPosition(0, 0).widget()
        templateMiniMapTileWidget = self.miniMap.layout().itemAtPosition(0, 0).widget()
        self.currentMapGrid().removeWidget(templateTileWidget)
        self.miniMap.layout().removeWidget(templateMiniMapTileWidget)
        templateTileWidget.deleteLater()
        templateMiniMapTileWidget.deleteLater()
        del templateTileWidget
        del templateMiniMapTileWidget

    def drawTiles(self, setup = False):
        if setup:
            self.clearGrid(self.currentMapGrid())
            self.clearGrid(self.miniMap.layout())
        else:
            # Whitewash current ones
            self.whitewashGrid(self.currentMapGrid())
            self.whitewashGrid(self.miniMap.layout())

        for y in range(len(self.map.map)):
            for x in range(len(self.map.map[y])):
                tile = self.map.getTile(x, y)
                if tile:
                    tileType = tile.tileType
                else:
                    tileType = None

                self.drawTile(x, y, tileType, setup)

    def menuLoadClicked(self):
        loadDialog = LoadDialog(self.mapEditor, self)
        if loadDialog.exec_():
            mapId = loadDialog.getChosenMapId()
            if mapId:
                # Load the map
                self.map = self.mapEditor.API.getMap(str(mapId))
                self.load()

    def load(self):
        # Ok to commit now
        if self.map.name and len(self.map.name) > 0:
            self.actionCommit.setEnabled(True)

        # Set the name
        self.mapTabs.setTabText(self.mapTabs.currentIndex(), self.map.name)

        # Set the size
        self.mapSizeWidthBox.setValue(len(self.map.map[0]))
        self.mapSizeHeightBox.setValue(len(self.map.map))

        # Draw the tiles
        self.drawTiles(setup = True)

    def createTileWidget(self, x, y, miniMap = False):
        if miniMap:
            parent = self.miniMap
        else:
            parent = self.currentMapGrid().parent()

        tile = ClickableLabel(parent)
        tile.setText("")
        tile.setScaledContents(True)

        if miniMap:
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(tile.sizePolicy().hasHeightForWidth())
            tile.setMinimumSize(QtCore.QSize(1, 1))
            tile.setMaximumSize(QtCore.QSize(MapEditorWindow.MAX_MINI_MAP_TILE_SIZE, MapEditorWindow.MAX_MINI_MAP_TILE_SIZE))

            tile.setObjectName("miniMapTile")
            tile.clicked.connect(self.miniMapClicked)

        else:
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(tile.sizePolicy().hasHeightForWidth())
            tile.setMinimumSize(QtCore.QSize(MapEditorWindow.CURRENT_TILE_SIZE, MapEditorWindow.CURRENT_TILE_SIZE))
            tile.setMaximumSize(QtCore.QSize(MapEditorWindow.CURRENT_TILE_SIZE, MapEditorWindow.CURRENT_TILE_SIZE))
            tile.setObjectName("tile")
            tile.x = x
            tile.y = y

            tile.clicked.connect(self.tileClicked)
            tile.hovered.connect(self.tileHovered)

        tile.setSizePolicy(sizePolicy)
        return tile

    def miniMapClicked(self):
        pass

    def currentMapGrid(self):
        return self.mapGrid.layout()
        # if self.mapGrid:
        #     return self.mapGrid
        #
        # children = self.mapTabs.currentWidget().findChildren(QtWidgets.QGridLayout, "gridLayout")
        # if children is not None and len(children) > 0:
        #     self.mapGrid = children[0]
        #     return self.mapGrid
        #
        # return None

    def resizeMapTiles(self):
        grid = self.currentMapGrid()
        for i in reversed(range(grid.count())):
            tileWidget = grid.itemAt(i).widget()
            tileWidget.setMinimumSize(QtCore.QSize(MapEditorWindow.CURRENT_TILE_SIZE, MapEditorWindow.CURRENT_TILE_SIZE))
            tileWidget.setMaximumSize(QtCore.QSize(MapEditorWindow.CURRENT_TILE_SIZE, MapEditorWindow.CURRENT_TILE_SIZE))

    def clearGrid(self, grid, skipFirst = False):
        for i in reversed(range(grid.count())):
            if skipFirst and i == 0:
                continue
            grid.itemAt(i).widget().setParent(None)

    def whitewashGrid(self, grid):
        for i in reversed(range(grid.count())):
            grid.itemAt(i).widget().setPixmap(self.mapEditor.GAME.TILE_TYPES[-1].getImage())

    def getCurrentToolName(self):
        return self.toolBoxButtons.checkedButton().objectName()

    def tileClicked(self, tileWidget, event):
        self.useTool(tileWidget)

    def tileHovered(self, tileWidget, event):
        if self.mapEditor.QAPP.mouseButtons() == QtCore.Qt.LeftButton:
            self.useTool(tileWidget)

    def useTool(self, tileWidget):
        currentTool = self.toolBoxButtons.checkedButton()

        if currentTool.objectName() == "pencilTool":
            self.usePencilTool(tileWidget)

    def usePencilTool(self, tileWidget):
        if self.paletteTiles.checkedButton():
            currentTileType = self.mapEditor.GAME.TILE_TYPES[self.paletteTiles.checkedButton().tileTypeId]
            if currentTileType:
                self.setTile(tileWidget.x, tileWidget.y, currentTileType)

    def setTile(self, x, y, tileType):
        currentTile = self.map.getTile(x, y)
        if currentTile:
            currentTile.tileType = tileType
        else:
            self.map.setTile(x, y, tileType.createTile(self, x, y))

        self.drawTile(x, y, tileType)

    def drawTile(self, x, y, tileType, setup = False):
        if setup:
            tileWidget = self.createTileWidget(x, y)
            self.currentMapGrid().addWidget(tileWidget, y, x)
            miniMapTileWidget = self.createTileWidget(x, y, miniMap = True)
            self.miniMap.layout().addWidget(miniMapTileWidget, y, x)
        else:
            tileWidget = self.currentMapGrid().itemAtPosition(y, x).widget()
            miniMapTileWidget = self.miniMap.layout().itemAtPosition(y, x).widget()

        if tileType:
            tileWidget.setPixmap(tileType.getImage())
            miniMapTileWidget.setPixmap(tileType.getImage())
        else:
            tileWidget.setPixmap(self.mapEditor.GAME.TILE_TYPES[-1].getImage())
            miniMapTileWidget.setPixmap(self.mapEditor.GAME.TILE_TYPES[-1].getImage())

    def createPalette(self):
        x = 1
        y = 0
        for tileType in self.mapEditor.GAME.TILE_TYPES.itervalues():
            tile = self.createPaletteTile(tileType)
            self.palette.layout().addWidget(tile, y, x)

            x += 1
            if x % 6 == 0:
                x = 0
                y += 1

    def createPaletteTile(self, tileType):
        paletteTile = QtWidgets.QToolButton(self.palette)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(paletteTile.sizePolicy().hasHeightForWidth())
        paletteTile.setSizePolicy(sizePolicy)
        paletteTile.setIconSize(QtCore.QSize(24, 24))
        paletteTile.setCheckable(True)
        paletteTile.setObjectName("paletteTile")
        paletteTile.setIcon(QtGui.QIcon(tileType.getImage()))
        paletteTile.tileTypeId = tileType.id
        self.paletteTiles.addButton(paletteTile)
        return paletteTile

    def newTileTypeClicked(self):
        newTileTypeDialog = NewTileTypeDialog(self.mapEditor, self)
        if newTileTypeDialog.exec_():
            tileType = newTileTypeDialog.getCreatedTileType()
            if tileType:
                # Add it to the list
                self.mapEditor.GAME.TILE_TYPES[tileType.id] = tileType

                # Clear the palette
                self.clearGrid(self.palette.layout(), skipFirst = True)

                # Redraw the palette
                self.createPalette()
