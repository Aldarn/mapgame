from PyQt5 import QtCore, QtGui, QtWidgets
from ui.load_dialog import Ui_LoadDialog

class LoadDialog(QtWidgets.QDialog, Ui_LoadDialog):
    def __init__(self, mapEditor, parent=None):
        self.mapEditor = mapEditor
        self.mapId = None

        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.loadButton.clicked.connect(self.load)
        self.cancelButton.clicked.connect(self.reject)

        self.populateList()

    def populateList(self):
        maps = self.mapEditor.API.getMaps()

        # Remove loading item
        self.mapList.clear()

        if maps is not None and len(maps) > 0:
            for map in maps:
                listItem = QtWidgets.QListWidgetItem(map["name"])
                listItem.setData(QtCore.Qt.UserRole, map["id"])
                self.mapList.addItem(listItem)
        else:
            self.mapList.addItem("No maps found!")

    def load(self):
        # Get the id of the selected map
        if self.mapList.selectedItems() is not None and len(self.mapList.selectedItems()) > 0:
            self.mapId = self.mapList.selectedItems()[0].data(QtCore.Qt.UserRole)

        self.accept()

    def getChosenMapId(self):
        return self.mapId