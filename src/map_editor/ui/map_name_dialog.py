# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../ui/map_name_dialog.ui'
#
# Created: Sat Jun 28 20:44:55 2014
#      by: PyQt5 UI code generator 5.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MapNameDialog(object):
    def setupUi(self, MapNameDialog):
        MapNameDialog.setObjectName("MapNameDialog")
        MapNameDialog.resize(400, 125)
        self.verticalLayout = QtWidgets.QVBoxLayout(MapNameDialog)
        self.verticalLayout.setSpacing(10)
        self.verticalLayout.setContentsMargins(10, 10, 10, 10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.descriptionLabel = QtWidgets.QLabel(MapNameDialog)
        self.descriptionLabel.setContentsMargins(0, 0, 0, 0)
        self.descriptionLabel.setIndent(10)
        self.descriptionLabel.setObjectName("descriptionLabel")
        self.verticalLayout.addWidget(self.descriptionLabel)
        spacerItem = QtWidgets.QSpacerItem(20, 5, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.form = QtWidgets.QWidget(MapNameDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.form.sizePolicy().hasHeightForWidth())
        self.form.setSizePolicy(sizePolicy)
        self.form.setObjectName("form")
        self.gridLayout = QtWidgets.QGridLayout(self.form)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout.setSpacing(10)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.mapNameLabel = QtWidgets.QLabel(self.form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mapNameLabel.sizePolicy().hasHeightForWidth())
        self.mapNameLabel.setSizePolicy(sizePolicy)
        self.mapNameLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.mapNameLabel.setObjectName("mapNameLabel")
        self.gridLayout.addWidget(self.mapNameLabel, 0, 0, 1, 1)
        self.mapNameBox = QtWidgets.QLineEdit(self.form)
        self.mapNameBox.setObjectName("mapNameBox")
        self.gridLayout.addWidget(self.mapNameBox, 0, 1, 1, 2)
        self.gridLayout.setColumnStretch(1, 1)
        self.verticalLayout.addWidget(self.form)
        self.buttonContainer = QtWidgets.QWidget(MapNameDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonContainer.sizePolicy().hasHeightForWidth())
        self.buttonContainer.setSizePolicy(sizePolicy)
        self.buttonContainer.setObjectName("buttonContainer")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.buttonContainer)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.cancelButton = QtWidgets.QPushButton(self.buttonContainer)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.saveButton = QtWidgets.QPushButton(self.buttonContainer)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.verticalLayout.addWidget(self.buttonContainer)

        self.retranslateUi(MapNameDialog)
        QtCore.QMetaObject.connectSlotsByName(MapNameDialog)

    def retranslateUi(self, MapNameDialog):
        _translate = QtCore.QCoreApplication.translate
        MapNameDialog.setWindowTitle(_translate("MapNameDialog", "Set Map Name"))
        self.descriptionLabel.setText(_translate("MapNameDialog", "Choose a name for the map."))
        self.mapNameLabel.setText(_translate("MapNameDialog", "Map Name:"))
        self.cancelButton.setText(_translate("MapNameDialog", "Cancel"))
        self.saveButton.setText(_translate("MapNameDialog", "Save"))

