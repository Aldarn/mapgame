# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../ui/load_dialog.ui'
#
# Created: Sat Jun 28 20:44:54 2014
#      by: PyQt5 UI code generator 5.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_LoadDialog(object):
    def setupUi(self, LoadDialog):
        LoadDialog.setObjectName("LoadDialog")
        LoadDialog.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(LoadDialog)
        self.verticalLayout.setSpacing(5)
        self.verticalLayout.setContentsMargins(10, 10, 10, 10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.mapList = QtWidgets.QListWidget(LoadDialog)
        self.mapList.setObjectName("mapList")
        item = QtWidgets.QListWidgetItem()
        self.mapList.addItem(item)
        self.verticalLayout.addWidget(self.mapList)
        self.buttonContainer = QtWidgets.QWidget(LoadDialog)
        self.buttonContainer.setObjectName("buttonContainer")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.buttonContainer)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.cancelButton = QtWidgets.QPushButton(self.buttonContainer)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.loadButton = QtWidgets.QPushButton(self.buttonContainer)
        self.loadButton.setObjectName("loadButton")
        self.horizontalLayout.addWidget(self.loadButton)
        self.verticalLayout.addWidget(self.buttonContainer)

        self.retranslateUi(LoadDialog)
        QtCore.QMetaObject.connectSlotsByName(LoadDialog)

    def retranslateUi(self, LoadDialog):
        _translate = QtCore.QCoreApplication.translate
        LoadDialog.setWindowTitle(_translate("LoadDialog", "Load Map"))
        __sortingEnabled = self.mapList.isSortingEnabled()
        self.mapList.setSortingEnabled(False)
        item = self.mapList.item(0)
        item.setText(_translate("LoadDialog", "Loading..."))
        self.mapList.setSortingEnabled(__sortingEnabled)
        self.cancelButton.setText(_translate("LoadDialog", "Cancel"))
        self.loadButton.setText(_translate("LoadDialog", "Load"))

