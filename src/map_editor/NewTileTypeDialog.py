import os

from PyQt5 import QtCore, QtGui, QtWidgets
from ui.new_tile_type_dialog import Ui_NewTileTypeDialog
from domain.TileType import TileType

class NewTileTypeDialog(QtWidgets.QDialog, Ui_NewTileTypeDialog):
    def __init__(self, mapEditor, parent=None):
        self.mapEditor = mapEditor
        self.imagePath = None
        self.tileType = None

        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.browseButton.clicked.connect(self.browse)
        self.createButton.clicked.connect(self.create)
        self.cancelButton.clicked.connect(self.reject)

    def browse(self):
        filePath = QtWidgets.QFileDialog.getOpenFileName(self, "Select Image", "../server/mapgame/images/tiles/")[0]
        self.imagePath = os.path.basename(filePath)
        self.selectedImageLabel.setText(self.imagePath)

    def create(self):
        if len(self.nameBox.text()) > 0 and self.imagePath:
            self.tileType = TileType(
                self.mapEditor.GAME,
                max(k for k, v in self.mapEditor.GAME.TILE_TYPES.iteritems() if v != 0) + 1,
                self.nameBox.text(),
                self.imagePath
            )

            self.mapEditor.API.createTileType(self.tileType)

            self.accept()

    def getCreatedTileType(self):
        return self.tileType
