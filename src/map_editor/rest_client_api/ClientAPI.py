import urllib
import urllib2
import json
import requests
import copy

from domain.Map import Map
from domain.TileType import TileType

class ClientAPI(object):
    MAP_URL = "http://lobster.bdholmes.com/mapgame/map"
    MAPS_URL = "http://lobster.bdholmes.com/mapgame/maps"
    TILE_TYPES_URL = "http://lobster.bdholmes.com/mapgame/tileTypes"
    CREATE_TILE_TYPE_URL = "http://lobster.bdholmes.com/mapgame/create/tileType"
    SAVE_MAP_URL = "http://lobster.bdholmes.com/mapgame/saveMap"
    NEW_MAP_ID_URL = "http://lobster.bdholmes.com/mapgame/newMapId"

    def __init__(self, client):
        self.client = client

    def getDataFromURL(self, url, **kwargs):
        if len(kwargs) > 0:
            url += "?"
            for key, value in kwargs.iteritems():
                url += key + "=" + value + "&"
            url = url[:-1]

        # TODO: Some kind of urlencoding?

        data = urllib2.urlopen(url).read()
        return json.loads(data)

    def sendDataToURL(self, url, dictData):
        # user_agent = "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)"
        # headers = {'User-Agent' : user_agent}

        # Urlencode the JSON data
        # data = urllib.urlencode(data)

        # Send the request
        # request = urllib2.Request(url, data, headers)
        # request = urllib2.Request(url, data)
        # response = urllib2.urlopen(request)

        # Get the response
        # responseMessage = response.read()
        # print responseMessage

        request = requests.post(url, data = dictData)
        print "sent data to " + url + " response: " + str(request.status_code) + ", other: " + str(request)
        with open("/Users/bholmes/Documents/resultPage.html", 'w') as resultPage:
            resultPage.write(request.text)

    def mapDataToObject(self, data, objClass):
        pass

    def getMap(self, mapId):
        data = self.getDataFromURL(ClientAPI.MAP_URL, id = mapId)
        return Map(self.client.GAME, data["mapData"], data["tileData"], data["npcData"], data["actionData"])

    def getMaps(self):
        return self.getDataFromURL(ClientAPI.MAPS_URL)

    def getNewMapId(self):
        return self.getDataFromURL(ClientAPI.NEW_MAP_ID_URL)

    def getTileTypes(self):
        data = self.getDataFromURL(ClientAPI.TILE_TYPES_URL)
        if data is not None:
            if isinstance(data, list):
                return {
                    tileTypeData["id"]: TileType(self.client.GAME, tileTypeData["id"], tileTypeData["name"], tileTypeData["image"])
                    for tileTypeData in data
                }
            return {
                data["id"]: TileType(self.client.GAME, data["id"], data["name"], data["image"])
            }

    def createTileType(self, tileType):
        tmpTileType = copy.deepcopy(tileType)
        del tmpTileType.game
        self.sendDataToURL(ClientAPI.CREATE_TILE_TYPE_URL, tmpTileType.__dict__)

    def saveMap(self, map, mapData):
        self.sendDataToURL(ClientAPI.SAVE_MAP_URL, {"id": map.id, "data": json.dumps(mapData)})
