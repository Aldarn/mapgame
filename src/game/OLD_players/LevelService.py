
# TODO: Can we synchronize the level up methods so only one person can level up at once?
# 		- this is mainly to avoid conflicts if people are levelling up around each other
class LevelService (object):
	BASE_LEVEL_LIMIT = 10

	def __init__ (self):
		pass

	# TODO: Calculate these values & store in LEVEL_EXPERIENCE_MAP
	def getAllLevelExperiencePoints(self):
		points = 0
		for level in range(1,100):
			diff = int(level + 300 * math.pow(2, float(level)/7) )
			points += diff
			str = "Level %d = %d" % (level + 1, points / 4)
			print str

	def getLevelExperiencePoints(self, level):
		return LEVEL_EXPERIENCE_MAP[level]

	def getLevelProgressPercentage(self, player):
		if player.level < BASE_LEVEL_LIMIT:
			levelExpDifference = getLevelExperiencePoints(player.level + 1) - getLevelExperiencePoints(player.level)
			levelExpProgress = player.experience - getLevelExperiencePoints(player.level)
			progressPercent = int(((double)levelExpProgress / (double)levelExpDifference) * 100)

		else:
			# TODO: Factor in the person with the lowest experience in the next bonus level that you have to overtake
			pass

		return progressPercent

	def doLevelUp(self, player, levelsGained):
		player.level += levelsGained

	def doBonusLevelUp(self, player, levelsGained, levelDownPlayer = None):
		pass

	def getBonusLevelTotalSlots(self, level):
		pass

	def getBonusLevelUsedSlots(self, level):
		pass

	def isBonusLevelSlotAvailable(self, level):
		pass

	def checkLevelUp(self, player):
		# Check if they have more exp than the next level up
		if player.getExperiencePoints < getLevelExperiencePoints(player.level + 1):
			return False

		# Figure out how many levels they went up
		levelsGained = 1
		while player.getExperiencePoints >= getLevelExperiencePoints(player.level + levelsGained + 1):
			levelsGained += 1

		# Level them up normally if they're below the base limit
		if (player.level + levelsGained) <= BASE_LEVEL_LIMIT:
			doLevelUp(player, levelsGained)
			return True

		# Attempt to level them up to the bonus level 
		for levelsGained in reverse(range(levelsGained, 1)):
			# Easy if there's slots available
			if isBonusLevelSlotAvailable(player.level + levelsGained):
				doBonusLevelUp(player, levelsGained)
				return True

			# TODO: Check if they have > experience than lowest guy in next level & swap them accordingly
			

		# Edge case check for players going from e.g. level 9 -> 11 and fail to get into 11, so fall back to 10
		if player.level < BASE_LEVEL_LIMIT and (player.level + levelsGained) > BASE_LEVEL_LIMIT:
			doLevelUp(player, BASE_LEVEL_LIMIT)
			return True

		return False

	LEVEL_EXPERIENCE_MAP = {
		1: 83,
		...
	}