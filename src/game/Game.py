class Game(object):
    TILE_TYPES = {} # Map of tile type id : object
    TILE_ACTIONS = []
    NPC_TYPES = []
    MAPS = []

    def __init__(self, client):
        self.client = client

        # Load game data e.g. tile types, npcs, etc
        self.load()

    def load(self):
        # Load TileTypes
        Game.TILE_TYPES = self.client.API.getTileTypes()