from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
	url(r'/$', views.index, name='/index'),
	url(r'/user', views.getUser, name='/user'),
    url(r'/player', views.getPlayer, name='/player'),
	url(r'/login', views.login, name='/login'),
	url(r'/map$', views.getMap, name='/map'),
	url(r'/maps', views.getMaps, name='/maps'),
    url(r'/newMapId', views.getNewMapId, name='/newMapId'),
    url(r'/saveMap', views.saveMap, name='/saveMap'),
	url(r'/tileTypes', views.getTileTypes, name='/tileTypes'),
	url(r'/create/tileType', views.createTileType, name='/create/tileType'),
    url(r'/npcTypes', views.getNPCTypes, name='/npcTypes'),
    url(r'/attackableNPCTypes', views.getAttackableNPCTypes, name='/attackableNPCTypes'),
    url(r'/npcs', views.getMapNPCs, name='/npcs')
)
