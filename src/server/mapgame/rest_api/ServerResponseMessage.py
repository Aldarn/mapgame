import json
from ..utils.utils import getFields

import logging
logger = logging.getLogger("mapgame")

class ServerResponseMessage (object):
    def __init__ (self, message):
        self.message = message

    def __str__ (self):
        asDict = {}
        fieldNames = getFields(self)

        for fieldName in fieldNames:
            asDict[fieldName] = getattr(self, fieldName)
        return json.dumps(asDict)