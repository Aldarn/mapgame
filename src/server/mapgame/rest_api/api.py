from django.db import connection
from ServerResponseMessage import ServerResponseMessage
import lobster.config as config
import os
import glob
import json
import re

class API (object):
    def __init__ (self):
        pass

    def query(self, queryStr, vals = None):
        cursor = connection.cursor()
        cursor.execute(queryStr, vals)
        results = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]

        if len(results) == 0:
            return None
        elif len(results) == 1:
            return results[0]

        return results

    def jsonQuery(self, queryStr, vals = None):
        return json.dumps(self.query(queryStr, vals))

    def login(self, user, password):
        loginResult = self.query("SELECT * FROM users WHERE username = %s AND password = %s", (user, password))

        if loginResult:
            # TODO: Set cookie
            return ServerResponseMessage("Success")
        return ServerResponseMessage("Fail")

    def getUser(self, user):
        return self.jsonQuery("SELECT * FROM users WHERE username = %s", user)

    def getPlayer(self, userId):
        return self.jsonQuery("SELECT * FROM players WHERE user_id = %s", userId)

    def getTileTypes(self):
        tileTypes = {}
        tileTypeResults = self.query("SELECT tile_types.*, tile_type_hit_rects.x, tile_type_hit_rects.y, "
            "tile_type_hit_rects.width, tile_type_hit_rects.height FROM tile_types LEFT JOIN "
            "tile_type_hit_rects ON tile_types.id = tile_type_hit_rects.tile_type_id")

        for tileTypeResult in tileTypeResults:
            if tileTypes.has_key(tileTypeResult.get("id")):
                tileType = tileTypes.get(tileTypeResult.get("id"))
            else:
                tileType = {
                    "name": tileTypeResult.get("name"),
                    "id": tileTypeResult.get("id"),
                    "image": tileTypeResult.get("image"),
                    "walkable": tileTypeResult.get("walkable")
                }
                tileTypes[tileType.get("id")] = tileType

            if tileTypeResult.get("x") is not None:
                tileTypeHitRect = {
                    "x": tileTypeResult.get("x"),
                    "y": tileTypeResult.get("y"),
                    "width": tileTypeResult.get("width"),
                    "height": tileTypeResult.get("height"),
                }

                if tileType.has_key("hit_rects"):
                    tileType.get("hit_rects").append(tileTypeHitRect)
                else:
                    tileType["hit_rects"] = [tileTypeHitRect]

        return json.dumps(tileTypes.values())

    def getMaps(self):
        mapFiles = glob.glob(os.path.join(config.SERVER_MAP_DIRECTORY, "*.json"))
        mapFileInfo = []

        for mapFilePath in mapFiles:
             with open(mapFilePath, 'r') as mapFile:
                mapData = json.loads(mapFile.read())["mapData"]
                mapFileInfo.append({"id": mapData["id"], "name": mapData["name"]})

        return json.dumps(mapFileInfo)

    def getMap(self, id):
        # TODO: Cache this
        mapFilePath = os.path.join(config.SERVER_MAP_DIRECTORY, id + ".json")
        if os.path.exists(mapFilePath):
            # Load the map json file
            with open(mapFilePath, 'r') as mapFile:
                mapJson = mapFile.read()

            # Decode the map json file
            mapJsonDict = json.loads(mapJson)

            return json.dumps(mapJsonDict)

        return ServerResponseMessage("No map found with id " + id + " at path " + mapFilePath)

    def getNewMapId(self):
        maxId = 0
        mapFiles = glob.glob(os.path.join(config.SERVER_MAP_DIRECTORY, "*.json"))
        for mapFilePath in mapFiles:
            getId = re.match(r".+\/(\d+)\.json", mapFilePath)
            if getId:
                id = getId.group(1)
                if int(id) > maxId:
                    maxId = int(id)

        return maxId + 1

    def saveMap(self, id, data):
        mapFilePath = os.path.join(config.SERVER_MAP_DIRECTORY, id + ".json")
        with open(mapFilePath, 'w') as mapFile:
            os.chmod(mapFilePath, 0777)
            mapFile.write(data)

    def createTileType(self, name, imagePath):
        self.query("INSERT INTO `tile_types` (`name`, `image`) VALUES (%s, %s)", (name, imagePath))

    def getNPCTypes(self):
        return self.query("SELECT * FROM npc_types")

    def getAttackbleNPCTypes(self):
        return self.query("SELECT * FROM attackable_npc_types")

    def getMapNPCs(self, mapId, playerId, getTypes = True):
        npcs = {}

        if getTypes:
            npcs["npcTypes"] = self.getNPCTypes();
            npcs["attackableNpcTypes"] = self.getAttackbleNPCTypes()

        npcs["mapNpcs"] = self.query(
            "SELECT npcs.*, "
            "attackable_npcs.id AS attackable_npc_id, attackable_npcs.attackable_type_id, attackable_npcs.health, attackable_npcs.respawn_time "
            "FROM npcs "
            "LEFT JOIN attackable_npcs ON npcs.id = attackable_npcs.npc_id AND attackable_npcs.player_id = %s "
            "WHERE npcs.map_id = %s", (playerId, mapId)
        )

        return json.dumps(npcs)