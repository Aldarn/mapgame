import os

def makeNonRelative(path):
    return os.path.join(os.path.dirname(__file__), path)

# Tile config
EDITOR_TILE_IMAGE_PATH = makeNonRelative("images/tiles")

# NPC config
NPC_IMAGE_PATH = makeNonRelative("images/npcs")

# Map config
SERVER_MAP_DIRECTORY = makeNonRelative("data/maps")