from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from rest_api.API import API
from rest_api.ServerResponseMessage import ServerResponseMessage

api = API()

def checkAuth (request):
    # TODO: Check they're logged in & can access this method
    pass

def index (request):
    return HttpResponse("<p>Lobster!</p><p><script type='text/javascript' id='WolframAlphaScript362210b908fc230d74a06a78a2ae2fdc' src='http://www.wolframalpha.com/widget/widget.jsp?id=362210b908fc230d74a06a78a2ae2fdc'></script></p>")

def login (request):
    response = ""
    if request.method == 'GET':
        user = request.GET["user"]
        password = request.GET["password"]
        response = api.login(user, password)

    return HttpResponse(response, content_type='application/json')

def getUser (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        user = request.GET["user"]
        response = api.getUser(user)

    return HttpResponse(response, content_type='application/json')

def getPlayer (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        userId = request.GET["id"]
        response = api.getPlayer(userId)

    return HttpResponse(response, content_type='application/json')

def getMap (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        mapId = request.GET["id"]
        response = api.getMap(mapId)

    return HttpResponse(response, content_type='application/json')

def getMaps (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        response = api.getMaps()

    return HttpResponse(response, content_type='application/json')

def getNewMapId (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        response = api.getNewMapId()

    return HttpResponse(response, content_type='application/json')

def getTileTypes (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        response = api.getTileTypes()

    return HttpResponse(response, content_type='application/json')

@csrf_exempt
def createTileType (request):
    checkAuth(request)

    if request.method == 'POST':
        name = str(request.POST["name"])
        imagePath = str(request.POST["imagePath"])
        api.createTileType(name, imagePath)

        return HttpResponse(ServerResponseMessage("Success."))

    return HttpResponse(ServerResponseMessage("Invalid usage."))

@csrf_exempt
def saveMap (request):
    checkAuth(request)

    if request.method == 'POST':
        api.saveMap(request.POST["id"], request.POST["data"])

        return HttpResponse(ServerResponseMessage("Success."))

    return HttpResponse(ServerResponseMessage("Invalid usage."))

def getNPCTypes (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        response = api.getNPCTypes()

    return HttpResponse(response, content_type='application/json')

def getAttackableNPCTypes (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        response = api.getAttackbleNPCTypes()

    return HttpResponse(response, content_type='application/json')

def getMapNPCs (request):
    checkAuth(request)

    response = ""
    if request.method == 'GET':
        mapId = request.GET["mapId"]
        playerId = request.GET["playerId"]
        getTypes = bool(request.GET["types"])
        response = api.getMapNPCs(mapId, playerId, getTypes)

    return HttpResponse(response, content_type='application/json')