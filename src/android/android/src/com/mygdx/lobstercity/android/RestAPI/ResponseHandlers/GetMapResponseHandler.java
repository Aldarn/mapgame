package com.mygdx.lobstercity.android.RestAPI.ResponseHandlers;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.mygdx.lobstercity.Domain.GameMap;
import com.mygdx.lobstercity.Domain.Player;
import com.mygdx.lobstercity.Interfaces.WantsMap;
import com.mygdx.lobstercity.Interfaces.WantsPlayer;

import org.apache.http.Header;
import org.json.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bholmes on 30/06/2014.
 *
 */
public class GetMapResponseHandler extends JsonHttpResponseHandler {

    WantsMap wantsMapObject;

    public GetMapResponseHandler(WantsMap wantsMapObject) {
        this.wantsMapObject = wantsMapObject;
    }

    @Override
    public void onStart() {
        // called before request is started
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        try {
            List<ArrayList<Integer>> tileData = new ArrayList<ArrayList<Integer>>();
            for (int y = 0; y < ((JSONArray)response.get("tileData")).length(); y++) {
                ArrayList<Integer> tiles = new ArrayList<Integer>();
                tileData.add(tiles);

                JSONArray tileDataY = ((JSONArray)((JSONArray)response.get("tileData")).get(y));
                for (int x = 0; x < tileDataY.length(); x++) {
                    tiles.add(tileDataY.getInt(x));
                }
            }

            GameMap gameMap = new GameMap(
                    (Integer)((JSONObject)response.get("mapData")).get("id"),
                    (String)((JSONObject)response.get("mapData")).get("name"),
                    tileData
            );

            wantsMapObject.onMapReceived(gameMap);
        } catch(Exception e) {
            System.out.println("Error creating map: " + e);
            e.printStackTrace();

            wantsMapObject.onMapFailed();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray errorResponse) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)

        wantsMapObject.onMapFailed();
    }

    @Override
    public void onRetry(int retryNo) {
        // called when request is retried
    }
}
