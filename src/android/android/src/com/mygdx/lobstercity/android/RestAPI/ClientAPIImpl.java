package com.mygdx.lobstercity.android.RestAPI;
import com.loopj.android.http.*;
import com.mygdx.lobstercity.Interfaces.ClientAPI;
import com.mygdx.lobstercity.Interfaces.WantsMap;
import com.mygdx.lobstercity.Interfaces.WantsMapNPCs;
import com.mygdx.lobstercity.Interfaces.WantsPlayer;
import com.mygdx.lobstercity.Interfaces.WantsTileTypes;
import com.mygdx.lobstercity.android.RestAPI.ResponseHandlers.GetMapNpcsResponseHandler;
import com.mygdx.lobstercity.android.RestAPI.ResponseHandlers.GetMapResponseHandler;
import com.mygdx.lobstercity.android.RestAPI.ResponseHandlers.GetPlayerResponseHandler;
import com.mygdx.lobstercity.android.RestAPI.ResponseHandlers.GetTileTypesResponseHandler;

/**
 * Created by bholmes on 30/06/2014.
 */
public class ClientAPIImpl implements ClientAPI {
    private static final String MAP_URL = "http://lobster.bdholmes.com/mapgame/map";
    private static final String MAPS_URL = "http://lobster.bdholmes.com/mapgame/maps";
    private static final String TILE_TYPES_URL = "http://lobster.bdholmes.com/mapgame/tileTypes";
    private static final String CREATE_TILE_TYPE_URL = "http://lobster.bdholmes.com/mapgame/create/tileType";
    private static final String SAVE_MAP_URL = "http://lobster.bdholmes.com/mapgame/saveMap";
    private static final String NEW_MAP_ID_URL = "http://lobster.bdholmes.com/mapgame/newMapId";
    private static final String PLAYER_URL = "http://lobster.bdholmes.com/mapgame/player";
    private static final String MAP_NPCS_URL = "http://lobster.bdholmes.com/mapgame/npcs";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public ClientAPIImpl() {
    }

    public static void get(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {
        client.get(url, params, responseHandler);
    }

    public static void post(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {
        client.post(url, params, responseHandler);
    }

    public void getPlayer(int id, WantsPlayer wantsPlayer) {
        RequestParams params = new RequestParams();
        params.put("id", id);

        JsonHttpResponseHandler responseHandler = new GetPlayerResponseHandler(wantsPlayer);

        get(ClientAPIImpl.PLAYER_URL, params, responseHandler);
    }

    public void getMap(int id, WantsMap wantsMap) {
        RequestParams params = new RequestParams();
        params.put("id", id);

        JsonHttpResponseHandler responseHandler = new GetMapResponseHandler(wantsMap);

        get(ClientAPIImpl.MAP_URL, params, responseHandler);
    }

    public void getTileTypes(WantsTileTypes wantsTileTypes) {
        RequestParams params = new RequestParams();

        JsonHttpResponseHandler responseHandler = new GetTileTypesResponseHandler(wantsTileTypes);

        get(ClientAPIImpl.TILE_TYPES_URL, params, responseHandler);
    }

    public void getMapNPCs(int id, int playerId, boolean types, WantsMapNPCs wantsMapNPCs) {
        RequestParams params = new RequestParams();
        params.put("mapId", id);
        params.put("playerId", playerId);
        params.put("types", types ? 1 : 0);

        JsonHttpResponseHandler responseHandler = new GetMapNpcsResponseHandler(wantsMapNPCs);

        get(ClientAPIImpl.MAP_NPCS_URL, params, responseHandler);
    }
}
