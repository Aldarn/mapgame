package com.mygdx.lobstercity.android.RestAPI.ResponseHandlers;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.mygdx.lobstercity.Domain.AttackableNPC;
import com.mygdx.lobstercity.Domain.AttackableNPCType;
import com.mygdx.lobstercity.Domain.NPC;
import com.mygdx.lobstercity.Domain.NPCType;
import com.mygdx.lobstercity.Interfaces.WantsMapNPCs;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bholmes on 13/07/2014.
 */
public class GetMapNpcsResponseHandler extends JsonHttpResponseHandler {

    WantsMapNPCs wantsMapNPCsObject;

    public GetMapNpcsResponseHandler(WantsMapNPCs wantsMapNPCsObject) {
        this.wantsMapNPCsObject = wantsMapNPCsObject;
    }

    @Override
    public void onStart() {
        // called before request is started
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        try {
            List<NPC> npcs = new ArrayList<NPC>();
            Map<Integer, NPCType> npcTypes = new HashMap<Integer, NPCType>();
            Map<Integer, AttackableNPCType> attackableNPCTypes = new HashMap<Integer, AttackableNPCType>();

            if(response.has("npcTypes")) {
                JSONArray jsonNPCTypes = response.getJSONArray("npcTypes");
                for(int i = 0; i < jsonNPCTypes.length(); i++) {
                    JSONObject jsonNPCType = jsonNPCTypes.getJSONObject(i);

                    npcTypes.put(
                            jsonNPCType.getInt("id"),
                            new NPCType(
                                    jsonNPCType.getInt("id"),
                                    jsonNPCType.getString("name"),
                                    (Integer)jsonNPCType.get("walkable") == 1,
                                    jsonNPCType.getInt("width"),
                                    jsonNPCType.getInt("height"),
                                    jsonNPCType.getString("sprite")
                            )
                    );
                }
            }

            if(response.has("attackableNpcTypes")) {
                JSONArray jsonAttackableNPCTypes = response.getJSONArray("attackableNpcTypes");
                for(int i = 0; i < jsonAttackableNPCTypes.length(); i++) {
                    JSONObject jsonAttackableNPCType = jsonAttackableNPCTypes.getJSONObject(i);

                    attackableNPCTypes.put(
                            jsonAttackableNPCType.getInt("id"),
                            new AttackableNPCType(
                                    jsonAttackableNPCType.getInt("id"),
                                    jsonAttackableNPCType.getInt("respawn_delay"),
                                    jsonAttackableNPCType.getInt("health"),
                                    jsonAttackableNPCType.getInt("damage"),
                                    jsonAttackableNPCType.getInt("defence"),
                                    jsonAttackableNPCType.getInt("accuracy"),
                                    jsonAttackableNPCType.getInt("agility"),
                                    jsonAttackableNPCType.getInt("critical"),
                                    jsonAttackableNPCType.getInt("block"),
                                    jsonAttackableNPCType.getInt("poison"),
                                    jsonAttackableNPCType.getInt("life_steal")
                            )
                    );
                }
            }

            JSONArray jsonNPCs = response.getJSONArray("mapNpcs");
            for(int i = 0; i < jsonNPCs.length(); i++) {
                JSONObject jsonNPC = jsonNPCs.getJSONObject(i);

                AttackableNPC attackableNPC = null;
                if(jsonNPC.get("attackable_npc_id").toString() != "null") {
                    attackableNPC = new AttackableNPC(
                            jsonNPC.getInt("attackable_npc_id"),
                            attackableNPCTypes.get(jsonNPC.getInt("attackable_type_id")),
                            jsonNPC.getInt("health"),
                            jsonNPC.getInt("respawn_time")
                    );
                }

                npcs.add(
                        new NPC(
                                jsonNPC.getInt("id"),
                                npcTypes.get(jsonNPC.getInt("npc_type_id")),
                                attackableNPC,
                                jsonNPC.getInt("map_id"),
                                jsonNPC.getInt("x"),
                                jsonNPC.getInt("y"),
                                jsonNPC.getInt("level")
                        )
                );
            }

            wantsMapNPCsObject.onMapNpcsReceived(npcTypes, attackableNPCTypes, npcs);
        } catch(Exception e) {
            System.out.println("Error creating map npcs: " + e);
            e.printStackTrace();

            wantsMapNPCsObject.onMapNpcsFailed();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray errorResponse) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
        System.out.println("failed to get tile types");
        System.out.println("*** fail 1");

        wantsMapNPCsObject.onMapNpcsFailed();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
        System.out.println("failed to get tile types");
        System.out.println("*** fail 2");

        wantsMapNPCsObject.onMapNpcsFailed();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
        System.out.println("failed to get map npcs");
        System.out.println("*** fail 3");

        wantsMapNPCsObject.onMapNpcsFailed();
    }

    @Override
    public void onRetry(int retryNo) {
        // called when request is retried
    }
}
