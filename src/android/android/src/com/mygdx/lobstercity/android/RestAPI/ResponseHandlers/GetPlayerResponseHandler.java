package com.mygdx.lobstercity.android.RestAPI.ResponseHandlers;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.mygdx.lobstercity.Domain.Player;
import com.mygdx.lobstercity.Interfaces.WantsPlayer;

import org.apache.http.Header;
import org.json.*;

/**
 * Created by bholmes on 30/06/2014.
 *
 *
 * TODO: Move this into the class that cares for now
 */
public class GetPlayerResponseHandler extends JsonHttpResponseHandler {

    WantsPlayer wantsPlayerObject;

    public GetPlayerResponseHandler(WantsPlayer wantsPlayerObject) {
        this.wantsPlayerObject = wantsPlayerObject;
    }

    @Override
    public void onStart() {
        // called before request is started
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        try {
            Player player = new Player(
                    (Integer)response.get("id"),
                    null,
                    (Integer)response.get("level"),
                    (Integer)response.get("experience"),
                    (Integer)response.get("stamina"),
                    (Integer)response.get("gold"),
                    (Integer)response.get("diamonds"),
                    (Integer)response.get("map"),
                    (Integer)response.get("map_x"),
                    (Integer)response.get("map_y")
            );

            wantsPlayerObject.onPlayerReceived(player);
        } catch(Exception e) {
            System.out.println("Error creating player: " + e);
            e.printStackTrace();

            wantsPlayerObject.onPlayerFailed();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray errorResponse) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
    }

    @Override
    public void onRetry(int retryNo) {
        // called when request is retried
    }
}
