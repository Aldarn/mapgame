package com.mygdx.lobstercity.android;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mygdx.lobstercity.Game;
import com.mygdx.lobstercity.android.RestAPI.ClientAPIImpl;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.hideStatusBar = true;
        config.useImmersiveMode = true;

		initialize(new Game(new ClientAPIImpl()), config);
	}
}
