package com.mygdx.lobstercity.android.RestAPI.ResponseHandlers;

import com.badlogic.gdx.math.Rectangle;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mygdx.lobstercity.Domain.TileType;
import com.mygdx.lobstercity.Interfaces.WantsTileTypes;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bholmes on 13/07/2014.
 */
public class GetTileTypesResponseHandler extends JsonHttpResponseHandler {

    WantsTileTypes wantsTileTypesObject;

    public GetTileTypesResponseHandler(WantsTileTypes wantsTileTypesObject) {
        this.wantsTileTypesObject = wantsTileTypesObject;
    }

    @Override
    public void onStart() {
        // called before request is started
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
        try {
            // TODO: Build the tile type objects
            List<TileType> tileTypes = new ArrayList<TileType>();

            for(int i = 0; i < response.length(); i++) {
                JSONObject jsonTileType = response.getJSONObject(i);

                List<Rectangle> hitRectangles = null;
                if(jsonTileType.has("hit_rects")) {
                    hitRectangles = new ArrayList<Rectangle>();
                    for (int j = 0; j < ((JSONArray) jsonTileType.get("hit_rects")).length(); j++) {
                        JSONObject hitRectData = ((JSONArray) jsonTileType.get("hit_rects")).getJSONObject(j);
                        Rectangle hitRect = new Rectangle(
                                hitRectData.getInt("x"),
                                hitRectData.getInt("y"),
                                hitRectData.getInt("width"),
                                hitRectData.getInt("height")
                        );
                        hitRectangles.add(hitRect);
                    }
                }

                boolean walkable = (Integer)jsonTileType.get("walkable") == 1;

                tileTypes.add(
                        new TileType(
                                (Integer)jsonTileType.get("id"),
                                (String)jsonTileType.get("name"),
                                (String)jsonTileType.get("image"),
                                walkable,
                                hitRectangles
                        )
                );
            }

            wantsTileTypesObject.onTileTypesReceived(tileTypes);
        } catch(Exception e) {
            System.out.println("Error creating tile types: " + e);
            e.printStackTrace();

            wantsTileTypesObject.onTileTypesFailed();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray errorResponse) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
        System.out.println("failed to get tile types");
        System.out.println("*** fail 1");

        wantsTileTypesObject.onTileTypesFailed();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
        System.out.println("failed to get tile types");
        System.out.println("*** fail 2");

        wantsTileTypesObject.onTileTypesFailed();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
        System.out.println("failed to get tile types");
        System.out.println("*** fail 3");

        wantsTileTypesObject.onTileTypesFailed();
    }

    @Override
    public void onRetry(int retryNo) {
        // called when request is retried
    }
}
