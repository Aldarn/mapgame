package com.mygdx.lobstercity;

/**
 * Created by bholmes on 02/07/2014.
 */
public class ScreenService {
    private Game game;
    private AbstractScreen currentScreen;

    private MenuScreen menuScreen;
    private MapScreen mapScreen;

    public ScreenService(Game game) {
        this.game = game;
        currentScreen = getScreen(ScreenType.MENU);
    }

    public AbstractScreen getCurrentScreen() {
        return currentScreen;
    }

    public AbstractScreen getBackScreen() {
        return getScreen(currentScreen.getBackScreen());
    }

    private AbstractScreen getScreen(ScreenType screen) {
        if(screen == ScreenType.MENU) {
            if(menuScreen != null) {
                return menuScreen;
            }
            menuScreen = new MenuScreen(game);
            return menuScreen;
        } else if (screen == ScreenType.MAP) {
            if(mapScreen != null) {
                return mapScreen;
            }
            mapScreen = new MapScreen(game);
            return mapScreen;
        }

        return null;
    }
}
