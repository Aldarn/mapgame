package com.mygdx.lobstercity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.SnapshotArray;
import com.mygdx.lobstercity.Domain.Actions.Action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bholmes on 07/12/2014.
 */
public class ActionService {
    public static Table PANEL;
    public static Map<Integer, List<Action>> ACTIONS = new HashMap<Integer, List<Action>>();
    private static Map<Integer, List<Cell>> ACTION_CELLS = new HashMap<Integer, List<Cell>>();

    private static Skin skin = new Skin(Gdx.files.internal("skins/uiskin.json"));

    public static void addActions(int npcId, List<Action> actions) {
        List<Cell> cells = new ArrayList<Cell>();
        for(Action action : actions) {
            cells.add(PANEL.add(new Label(action.getLabel(), skin)).fillX());
            PANEL.row();
        }
        ACTIONS.put(npcId, actions);
        ACTION_CELLS.put(npcId, cells);
    }

    public static void clearActions(int npcId) {
        for(Cell cell : ACTION_CELLS.get(npcId)) {
            cell.setActor(null);
            PANEL.getCells().removeIndex(PANEL.getCells().indexOf(cell, false));
        }
        ACTIONS.remove(npcId);
        ACTION_CELLS.remove(npcId);
    }
}
