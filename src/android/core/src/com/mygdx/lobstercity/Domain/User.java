package com.mygdx.lobstercity.Domain;

/**
 * Created by bholmes on 30/06/2014.
 */
public class User {
    private final String username;
    private final String email;

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public String getUsername() {
        return this.username;
    }

    public String getEmail() {
        return this.email;
    }
}
