package com.mygdx.lobstercity.Domain;

/**
 * Created by bholmes on 23/11/2014.
 */
public class NPCType {
    private int id;
    private String name;
    private boolean walkable;
    private int width;
    private int height;
    private String mapImagesPath;

    public NPCType(int id, String name, boolean walkable, int width, int height, String mapImagesPath) {
        this.id = id;
        this.name = name;
        this.walkable = walkable;
        this.width = width;
        this.height = height;
        this.mapImagesPath = mapImagesPath;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getMapImagesPath() {
        return mapImagesPath;
    }
}
