package com.mygdx.lobstercity.Domain;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by bholmes on 15/11/2014.
 */
public class SpriteAnimation {
    Animation animation;
    Texture sheet;
    TextureRegion[] frames;
    TextureRegion currentFrame;

    float stateTime;

    public SpriteAnimation(float speed, SpriteState spriteState) {
        sheet = new Texture(Gdx.files.internal(spriteState.getFilePath()));
        TextureRegion[][] tmp = TextureRegion.split(sheet, sheet.getWidth() / spriteState.getColumns(), sheet.getHeight() / spriteState.getRows());
        frames = new TextureRegion[spriteState.getColumns() * spriteState.getRows()];
        int index = 0;
        for (int i = 0; i < spriteState.getRows(); i++) {
            for (int j = 0; j < spriteState.getRows(); j++) {
                frames[index++] = tmp[i][j];
            }
        }
        currentFrame = frames[0];
        animation = new Animation(speed, frames);
        stateTime = 0f;
    }

    public void render(Batch spriteBatch, float x, float y) {
        stateTime += Gdx.graphics.getDeltaTime();
        currentFrame = animation.getKeyFrame(stateTime, true);
        spriteBatch.draw(currentFrame, x, y);
    }

    public void render(Batch spriteBatch, float x, float y, float rotation) {
        stateTime += Gdx.graphics.getDeltaTime();
        currentFrame = animation.getKeyFrame(stateTime, true);
        spriteBatch.draw(
                currentFrame,
                x,
                y,
                currentFrame.getRegionWidth() / 2.0f,
                currentFrame.getRegionHeight() / 2.0f,
                currentFrame.getRegionWidth(),
                currentFrame.getRegionHeight(),
                1f,
                1f,
                rotation,
                false
        );
    }

    public float getWidth() {
        return currentFrame.getRegionWidth();
    }

    public float getHeight() {
        return currentFrame.getRegionHeight();
    }
}
