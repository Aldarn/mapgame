package com.mygdx.lobstercity.Domain;

import java.util.List;

/**
 * Created by bholmes on 10/07/2014.
 */
public class EquippableItem {
    private final EquippableItemType equippableItemType;
    private int health;
    private int damage;
    private int defence;
    private int accuracy;
    private int agility;
    private int critical;
    private int block;
    private int poison;
    private int lifeSteal;

    public EquippableItem(EquippableItemType equippableItemType, int health, int damage, int defence,
            int accuracy, int agility, int critical, int block, int poison, int lifeSteal) {
        this.equippableItemType = equippableItemType;
        this.health = health;
        this.damage = damage;
        this.defence = defence;
        this.accuracy = accuracy;
        this.agility = agility;
        this.critical = critical;
        this.block = block;
        this.poison = poison;
        this.lifeSteal = lifeSteal;
    }
}
