package com.mygdx.lobstercity.Domain;


import com.badlogic.gdx.math.Rectangle;

/**
 * Created by bholmes on 30/06/2014.
 */

public class Player extends LobsterDataObject {
    private final User user;
    private final int level;
    private final int experience;
    private final int stamina;
    private final int gold;
    private final int diamonds;
    private final int map;

    public Player(int id, User user, int level, int experience, int stamina, int gold, int diamonds,
            int map, int mapX, int mapY) {
        super(id, 14, 14, mapX, mapY, 2.5f, "sprites/player");
        this.user = user;
        this.level = level;
        this.experience = experience;
        this.stamina = stamina;
        this.gold = gold;
        this.diamonds = diamonds;
        this.map = map;
    }

    public User getUser() {
        return this.user;
    }

    public int getLevel() {
        return this.level;
    }

    public int getExperience() { return this.experience; }

    public int getStamina() { return this.stamina; }

    public int getGold() { return this.gold; }

    public int getDiamonds() { return this.diamonds; }

    public int getMap() { return this.map; }
}
