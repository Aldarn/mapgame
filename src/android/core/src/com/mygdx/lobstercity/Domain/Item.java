package com.mygdx.lobstercity.Domain;

import java.util.List;

/**
 * Created by bholmes on 09/07/2014.
 */
public class Item {
    private Player owner;
    private final ItemType itemType;

    private EquippableItem equippableItem;
    private QuestItem questItem;
    private UsableItem usableItem;

    public Item(Player owner, ItemType itemType, EquippableItem equippableItem, QuestItem questItem, UsableItem usableItem) {
        this.owner = owner;
        this.itemType = itemType;
        this.equippableItem = equippableItem;
        this.questItem = questItem;
        this.usableItem = usableItem;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public EquippableItem getEquippableItem() {
        return equippableItem;
    }

    public QuestItem getQuestItem() {
        return questItem;
    }

    public UsableItem getUsableItem() {
        return usableItem;
    }

    /**
     * Returns a list of actions that can be performed on this item e.g. equip, drop, etc.
     *
     * @return
     */
    public List<ItemAction> getActions() {
        return null;
    }
}
