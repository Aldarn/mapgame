package com.mygdx.lobstercity.Domain;

import com.badlogic.gdx.scenes.scene2d.Actor;

import java.lang.reflect.Method;

/**
 * Created by bholmes on 10/07/2014.
 */
public abstract class ItemAction {
    private final Actor actionWidget;

    public ItemAction(Actor actionWidget) {
        this.actionWidget = actionWidget;
    }

    public Actor getWidget() {
        return actionWidget;
    }

    public abstract void run();
}
