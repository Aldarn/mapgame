package com.mygdx.lobstercity.Domain;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.mygdx.lobstercity.Domain.LobsterObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bholmes on 23/11/2014.
 */
public class LobsterTiledMapRenderer extends OrthogonalTiledMapRenderer {
    public static List<LobsterObject> NPCS = new ArrayList<LobsterObject>();
    private int npcLayer = 1;
    private SpriteBatch npcSpriteBatch = new SpriteBatch();

    public LobsterTiledMapRenderer(TiledMap map, float unitScale, List<LobsterObject> npcs) {
        super(map, unitScale);
        this.NPCS = npcs;
    }

    public void addNpc(LobsterObject npc){
        NPCS.add(npc);
    }

    public void render(Camera camera, float delta) {
        int currentLayer = 0;
        for (MapLayer layer : map.getLayers()) {
            if (layer.isVisible()) {
                if (layer instanceof TiledMapTileLayer) {
                    if(currentLayer == npcLayer) {
                        // npcSpriteBatch.begin();
                        for(LobsterObject npc : NPCS) {
                            npc.render(camera, npcSpriteBatch, delta);
                        }
                        // npcSpriteBatch.end();
                    } else {
                        beginRender();
                        renderTileLayer((TiledMapTileLayer)layer);
                        endRender();
                    }
                } else {
                    for (MapObject object : layer.getObjects()) {
                        renderObject(object);
                    }
                }
            }

            currentLayer++;
        }
    }
}