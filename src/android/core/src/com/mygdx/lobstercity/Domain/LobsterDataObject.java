package com.mygdx.lobstercity.Domain;

import com.badlogic.gdx.math.Rectangle;
import com.sun.media.jfxmedia.events.PlayerStateEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bholmes on 16/11/2014.
 */
public class LobsterDataObject {
    protected final int id;
    protected float width;
    protected float height;
    protected int mapX;
    protected int mapY;
    protected float moveSpeed = 2;
    protected String mapImagesPath;

    public LobsterDataObject(int id, float width, float height, int mapX, int mapY, float moveSpeed,
        String mapImagesPath) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.mapX = mapX;
        this.mapY = mapY;
        this.moveSpeed = moveSpeed;
        this.mapImagesPath = mapImagesPath;
    }

    public int getId() { return id; }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public int getMapX() {
        return mapX;
    }

    public int getMapY() {
        return mapY;
    }

    public float getMoveSpeed() { return moveSpeed; }

    /**
     * TODO: Refactor this to be generic and load the sheets from a json file.
     *
     * @return
     */
    public SpriteAnimator createAnimator () {
        List<SpriteState> playerStates = new ArrayList<SpriteState>();
        SpriteState playerStandingState = new SpriteState(LobsterObject.State.STOPPED, mapImagesPath + "/standing.png", 1, 1);
        playerStates.add(playerStandingState);
        SpriteState playerWalkingState = new SpriteState(LobsterObject.State.WALKING, mapImagesPath + "/walking.png", 4, 4);
        playerStates.add(playerWalkingState);

        return new SpriteAnimator(0.05f, playerStates);
    }
}
