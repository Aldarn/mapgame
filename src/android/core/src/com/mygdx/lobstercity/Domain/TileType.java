package com.mygdx.lobstercity.Domain;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bholmes on 13/07/2014.
 */
public class TileType {
    private int id;
    private String name;
    private String imageName;
    private boolean walkable;
    private List<Rectangle> hitRectangles;

    public TileType(int id, String name, String imagePath, boolean walkable, List<Rectangle> hitRectangles) {
        this.id = id;
        this.name = name;
        this.imageName = imagePath.split("\\.(?=[^\\.]+$)")[0];
        this.walkable = walkable;
        this.hitRectangles = hitRectangles;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public void setWalkable(boolean walkable) {
        this.walkable = walkable;
    }

    public List<Rectangle> getHitRectangles() { return hitRectangles; }

    /**
     * Creates a libgdx tile with the associated image for this tile type.
     *
     * @param atlas
     * @return
     */
    public StaticTiledMapTile getTile(TextureAtlas atlas) {
        TextureAtlas.AtlasRegion tileRegion = atlas.findRegion(imageName);
        return (tileRegion == null) ? new StaticTiledMapTile(atlas.findRegion("emptyTile")) : new StaticTiledMapTile(tileRegion);
    }
}
