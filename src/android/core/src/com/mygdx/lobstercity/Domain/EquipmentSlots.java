package com.mygdx.lobstercity.Domain;

/**
 * Created by bholmes on 10/07/2014.
 */
public enum EquipmentSlots {
    HEAD("Head"),
    BODY("Body"),
    LEFT_ARM("Left Arm"),
    RIGHT_ARM("Right Arm"),
    LEGS("Legs"),
    FEET("Feet");

    private String name;

    private EquipmentSlots(String name) {
        this.name = name;
    }
}
