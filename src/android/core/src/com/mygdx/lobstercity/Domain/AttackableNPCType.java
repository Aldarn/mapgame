package com.mygdx.lobstercity.Domain;

/**
 * Created by bholmes on 23/11/2014.
 */
public class AttackableNPCType {
    private int id;
    private int respawnDelay;
    private int health;
    private int damage;
    private int defence;
    private int accuracy;
    private int agility;
    private int critical;
    private int block;
    private int poison;
    private int lifeSteal;

    public AttackableNPCType(int id, int respawnDelay, int health, int damage, int defence,
        int accuracy, int agility, int critical, int block, int poison, int lifeSteal) {
        this.id = id;
        this.respawnDelay = respawnDelay;
        this.health = health;
        this.damage = damage;
        this.defence = defence;
        this.accuracy = accuracy;
        this.agility = agility;
        this.critical = critical;
        this.block = block;
        this.poison = poison;
        this.lifeSteal = lifeSteal;
    }

    public int getId() {
        return id;
    }

    public int getRespawnDelay() {
        return respawnDelay;
    }

    public int getHealth() {
        return health;
    }

    public int getDamage() {
        return damage;
    }

    public int getDefence() {
        return defence;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public int getAgility() {
        return agility;
    }

    public int getCritical() {
        return critical;
    }

    public int getBlock() {
        return block;
    }

    public int getPoison() {
        return poison;
    }

    public int getLifeSteal() {
        return lifeSteal;
    }
}
