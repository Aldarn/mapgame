package com.mygdx.lobstercity.Domain;

/**
 * Created by bholmes on 23/11/2014.
 */
public class AttackableNPC {
    private int id;
    private AttackableNPCType attackableNPCType;
    private int health;
    private int respawnTime;

    public AttackableNPC(int id, AttackableNPCType attackableNPCTypeId, int health, int respawnTime) {
        this.id = id;
        this.attackableNPCType = attackableNPCTypeId;
        this.health = health;
        this.respawnTime = respawnTime;
    }

    public int getId() {
        return id;
    }

    public AttackableNPCType getAttackableNPCType() {
        return attackableNPCType;
    }

    public int getHealth() {
        return health;
    }

    public int getRespawnTime() {
        return respawnTime;
    }
}
