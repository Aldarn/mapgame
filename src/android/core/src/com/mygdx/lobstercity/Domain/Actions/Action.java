package com.mygdx.lobstercity.Domain.Actions;

/**
 * Created by bholmes on 29/11/2014.
 */
public interface Action {
    public void execute();

    public String getLabel();
}
