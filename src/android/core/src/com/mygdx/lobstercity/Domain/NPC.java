package com.mygdx.lobstercity.Domain;

import com.mygdx.lobstercity.Domain.Actions.Action;
import com.mygdx.lobstercity.Domain.Actions.AttackAction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by bholmes on 23/11/2014.
 */
public class NPC extends LobsterDataObject {
    private int map;
    private NPCType npcType;
    private AttackableNPC attackableNPC;
    private int level;
    private List<Action> actions;

    public NPC(int id, NPCType npcType, AttackableNPC attackableNPC, int map, int mapX, int mapY, int level) {
        super(id, npcType.getWidth(), npcType.getHeight(), mapX, mapY, 2f, npcType.getMapImagesPath());

        this.map = map;
        this.npcType = npcType;
        this.attackableNPC = attackableNPC;
        this.level = level;

        generateActions();
    }

    private void generateActions() {
        actions = new ArrayList<Action>();

        if(this.level > 0) {
            actions.add(new AttackAction(this));
        }
    }

    public int getMap() { return this.map; }

    public NPCType getNpcType() {
        return npcType;
    }

    public AttackableNPC getAttackableNPC() {
        return attackableNPC;
    }

    public int getLevel() {
        return level;
    }

    public List<Action> getActions() {
        return actions;
    }
}

