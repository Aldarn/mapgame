package com.mygdx.lobstercity.Domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bholmes on 07/07/2014.
 */
public class GameMap {
    private final int id;
    private final String name;
    private final List<ArrayList<Integer>> tileTypes;

    private final int width;
    private final int height;

    public GameMap(int id, String name, List<ArrayList<Integer>> tileData) {
        this.id = id;
        this.name = name;
        this.tileTypes = tileData;

        this.width = tileData.get(0).size();
        this.height = tileData.size();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<ArrayList<Integer>> tileTypes() {
        return tileTypes;
    }

    public int getTileTypeId(int x, int y) {
        return tileTypes.get(y).get(x);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getTileCount() { return width * height; }
}
