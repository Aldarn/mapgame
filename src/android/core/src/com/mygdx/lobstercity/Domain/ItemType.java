package com.mygdx.lobstercity.Domain;

/**
 * Created by bholmes on 10/07/2014.
 */
public class ItemType {
    private final String name;
    private final String image;
    private final boolean tradable;

    public ItemType(String name, String image, boolean tradable) {
        this.name = name;
        this.image = image;
        this.tradable = tradable;
    }
}
