package com.mygdx.lobstercity.Domain;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.lobstercity.ActionService;
import com.mygdx.lobstercity.Game;
import com.mygdx.lobstercity.MenuScreen;

import java.util.List;

/**
 * Created by bholmes on 16/11/2014.
 */
public class LobsterObject {

    protected LobsterDataObject lobsterDataObject;
    protected SpriteAnimator animator;

    protected double currentMapX;
    protected double currentMapY;
    protected Vector3 worldLocation = new Vector3();
    protected float currentRotation = 180;
    protected boolean actionable = false;

    private ShapeRenderer shapeRenderer = new ShapeRenderer();

    public enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }
    protected Direction currentDirection = Direction.RIGHT;

    public enum State {
        STOPPED,
        WALKING
    }
    protected State currentState = State.STOPPED;

    protected boolean hitObject = false;

    public LobsterObject(LobsterDataObject lobsterDataObject, Camera camera) {
        this.lobsterDataObject = lobsterDataObject;
        this.animator = lobsterDataObject.createAnimator();

        this.currentMapX = lobsterDataObject.getMapX();
        this.currentMapY = lobsterDataObject.getMapY();

        updateWorldLocation(camera);
    }

    public void render(Camera camera, Batch batch, float delta) {
        // Move the object
        if(currentState == State.WALKING) {
            move(camera, delta);
        }

        // Debugging hack to always see hit tests even when not moving
        checkMove(camera, new Vector3((float)currentMapX, (float)currentMapY, 0));

        // Update the location relative to the world - must call for all objects as the camera could
        // move requiring this object to update its relative world location
        updateWorldLocation(camera);

        // Render object
        animator.render(currentState, batch, worldLocation.x, worldLocation.y, currentRotation);
    }

    public void updateWorldLocation(Camera camera){
        worldLocation.set((float) currentMapX, (float) currentMapY, 0);
        camera.project(worldLocation);
    }

    public LobsterDataObject getLobsterDataObject() {
        return lobsterDataObject;
    }

    public void setLobsterDataObject(LobsterDataObject lobsterDataObject) {
        this.lobsterDataObject = lobsterDataObject;
    }

    public SpriteAnimator getAnimator() {
        return animator;
    }

    public void setAnimator(SpriteAnimator animator) {
        this.animator = animator;
    }

    public boolean isActionable() {
        return actionable;
    }

    public void setActionable(boolean actionable) {
        this.actionable = actionable;
    }

    public double getMoveSpeed(float delta) {
        // TODO: Modify move speed according to agility
        return lobsterDataObject.getMoveSpeed() * delta;
    }

    public Direction getCurrentDirection() {
        return currentDirection;
    }

    public void setCurrentDirection(Direction direction) {
        this.currentDirection = direction;
    }

    public double getCurrentMapX() { return this.currentMapX; }

    public double getCurrentMapY() { return this.currentMapY; }

    public void move(Camera camera, float delta) {
        Vector3 newLocation = new Vector3((float)currentMapX, (float)currentMapY, 0);
        switch(currentDirection) {
            case UP:
                newLocation = moveUp(delta);
                break;
            case DOWN:
                newLocation = moveDown(delta);
                break;
            case LEFT:
                newLocation = moveLeft(delta);
                break;
            case RIGHT:
                newLocation = moveRight(delta);
                break;
        }

        checkMove(camera, new Vector3(newLocation.x, newLocation.y, newLocation.z));
        if(!hitObject) {
            currentMapX = newLocation.x;
            currentMapY = newLocation.y;
        } else {
            stop();
        }
    }

    public Vector3 moveUp(float delta) {
        return new Vector3((float)currentMapX, (float)(currentMapY + getMoveSpeed(delta)), 0);
    }

    public Vector3 moveDown(float delta) {
        return new Vector3((float)currentMapX, (float)(currentMapY - getMoveSpeed(delta)), 0);
    }

    public Vector3 moveLeft(float delta) {
        return new Vector3((float)(currentMapX - getMoveSpeed(delta)), (float)currentMapY, 0);
    }

    public Vector3 moveRight(float delta) {
        return new Vector3((float)(currentMapX + getMoveSpeed(delta)), (float)currentMapY, 0);
    }

    public void movedUp() {
        currentDirection = Direction.UP;
        currentRotation = 270;
        currentState = State.WALKING;
    }

    public void movedDown() {
        currentDirection = Direction.DOWN;
        currentRotation = 90;
        currentState = State.WALKING;
    }

    public void movedLeft() {
        currentDirection = Direction.LEFT;
        currentRotation = 0;
        currentState = State.WALKING;
    }

    public void movedRight() {
        currentDirection = Direction.RIGHT;
        currentRotation = 180;
        currentState = State.WALKING;
    }

    public void stop() {
        currentState = State.STOPPED;
    }

    /**
     * TODO: Move to common utils class?
     */
    private void checkMove(Camera camera, Vector3 newLocation) {
        // Reset hit flag
        hitObject = false;

        // Project to the new world location
        camera.project(newLocation);

        // Hitbox in actual pixels in the image
        Rectangle newWorldRect = getHitRect(newLocation);

        drawRect(newWorldRect, 0, 0, 1, 0.25f);

        // Loop each tile around this player
        for(int tileX = (int)currentMapX - 1; tileX <= (int)currentMapX + 1; tileX++) {
            for(int tileY = (int)currentMapY - 1; tileY <= (int)currentMapY + 1; tileY++) {
                Rectangle tileRect = getTileWorldRectangle(camera, tileX, tileY);
                if(newWorldRect.overlaps(tileRect)) {
                    drawRect(tileRect, 0, 1, 0, 0.25f);

                    // Check tile hit rects and do a hit test on each
                    List<Rectangle> hitRects = Game.TILE_TYPES.get(Game.MAP.getTileTypeId(tileX, Game.MAP.getHeight() - 1 - tileY)).getHitRectangles();
                    if(hitRects != null) {
                        for (Rectangle tileHitRect : hitRects) {
                            Rectangle tileHitWorldRect = getTileWorldHitRectangle(camera, tileX, tileY,
                                    tileHitRect.x, tileHitRect.y, tileHitRect.width, tileHitRect.height);

                            if (newWorldRect.overlaps(tileHitWorldRect)) {
                                hitObject = true;
                                break;
                            } else {
                                drawRect(tileHitWorldRect, 1, 0, 0, 0.25f);
                            }
                        }
                    }
                } else {
                    drawRect(tileRect, 1, 1, 0, 0.25f);
                }

                if(hitObject) {
                    break;
                }
            }

            if(hitObject) {
                break;
            }
        }

        // Check NPC's
        // These should probably be stored somewhere smarter?
        // TODO: Implement spacial partition data structure so we don't need to check ALL npc's each frame
        for(LobsterObject npc : LobsterTiledMapRenderer.NPCS) {
            if(this == npc) {
                continue;
            }

            if(!hitObject) {
                if (npc.getHitRect(npc.getWorldLocation()).overlaps(newWorldRect)) {
                    hitObject = true;
                }
            }

            if(lobsterDataObject instanceof Player) {
                Rectangle npcActionRect = npc.getActionRect(npc.getWorldLocation());
                drawRect(npcActionRect, 1, 1, 1, 0.25f);

                if (npcActionRect.overlaps(newWorldRect)) {
                    if(!npc.isActionable()) {
                        // Register the npc's available actions
                        ActionService.addActions(npc.getLobsterDataObject().getId(), ((NPC) npc.getLobsterDataObject()).getActions());
                        npc.setActionable(true);
                    }
                } else if(npc.isActionable()) {
                    // Remove the npc's actions
                    ActionService.clearActions(npc.getLobsterDataObject().getId());
                    npc.setActionable(false);
                }
            }
        }
    }

    private void drawRect(Rectangle rect, float r, float g, float b, float a) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(r, g, b, a);
        shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    private Rectangle getTileWorldRectangle(Camera camera, float tileX, float tileY) {
        Vector3 tileWorldLocation = new Vector3(tileX, tileY, 0);
        camera.project(tileWorldLocation);
        Vector3 tileWorldDimensions = new Vector3(tileX + 1, tileY + 1, 0);
        camera.project(tileWorldDimensions);

        return new Rectangle(tileWorldLocation.x, tileWorldLocation.y,
            tileWorldDimensions.x - tileWorldLocation.x, tileWorldDimensions.y - tileWorldLocation.y);
    }

    private Rectangle getTileWorldHitRectangle(Camera camera, float tileX, float tileY, float hitX,
            float hitY, float hitWidth, float hitHeight) {
        Vector3 hitWorldLocation = new Vector3(tileX + (hitX / 32), tileY + (hitY / 32), 0);
        camera.project(hitWorldLocation);
        Vector3 hitWorldDimensions = new Vector3(tileX + (hitX / 32) + (hitWidth / 32), tileY + (hitY / 32) + (hitHeight / 32), 0);
        camera.project(hitWorldDimensions);

        return new Rectangle(hitWorldLocation.x, hitWorldLocation.y,
            hitWorldDimensions.x - hitWorldLocation.x, hitWorldDimensions.y - hitWorldLocation.y);
    }

    private Vector3 getWorldLocation() {
        return worldLocation;
    }

    private Rectangle getHitRect(Vector3 location) {
        // Hitbox in actual pixels in the image
        // TODO: These dimensions should be defined by their assets
        Vector3 imageHitLocation = new Vector3(14, 15, 0);
        Vector3 imageHitDimensions = new Vector3(35 - 14, 33 - 15, 0);

        // Create the world hitbox
        return new Rectangle(
                location.x + imageHitLocation.x,
                location.y + imageHitLocation.y,
                imageHitDimensions.x,
                imageHitDimensions.y

        );
    }

    /**
     * Creates a rectangle to be used for detecting
     *
     * @param location
     * @return
     */
    private Rectangle getActionRect(Vector3 location) {
        // Create the world hitbox
        return new Rectangle(
                location.x - 25,
                location.y - 25,
                animator.getWidth(currentState) + 50,
                animator.getHeight(currentState) + 50
        );
    }
}
