package com.mygdx.lobstercity.Domain;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bholmes on 15/11/2014.
 */
public class SpriteAnimator {

    private Map<LobsterObject.State, SpriteAnimation> spriteAnimations = new HashMap<LobsterObject.State, SpriteAnimation>();

    public SpriteAnimator(float speed, List<SpriteState> spriteStates) {
        for(SpriteState spriteState : spriteStates) {
            this.spriteAnimations.put(spriteState.getObjectState(), new SpriteAnimation(speed, spriteState));
        }
    }

    public void render(LobsterObject.State state, Batch spriteBatch, float x, float y) {
        spriteBatch.begin();
        spriteAnimations.get(state).render(spriteBatch, x, y);
        spriteBatch.end();
    }

    public void render(LobsterObject.State state, Batch spriteBatch, float x, float y, float rotation) {
        spriteBatch.begin();
        spriteAnimations.get(state).render(spriteBatch, x, y, rotation);
        spriteBatch.end();
    }

    public float getWidth(LobsterObject.State state) {
        return spriteAnimations.get(state).getWidth();
    }

    public float getHeight(LobsterObject.State state) {
        return spriteAnimations.get(state).getHeight();
    }
}
