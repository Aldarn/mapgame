package com.mygdx.lobstercity.Domain;

/**
 * Created by bholmes on 15/11/2014.
 */
public class SpriteState {
    private final LobsterObject.State objectState;
    private final String filePath;
    private final int columns;
    private final int rows;

    public SpriteState(LobsterObject.State objectState, String filePath, int columns, int rows) {
        this.objectState = objectState;
        this.filePath = filePath;
        this.columns = columns;
        this.rows = rows;
    }

    public LobsterObject.State getObjectState() {
        return objectState;
    }

    public String getFilePath() {
        return filePath;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }
}
