package com.mygdx.lobstercity.Domain.Actions;

import com.mygdx.lobstercity.Domain.LobsterObject;
import com.mygdx.lobstercity.Domain.NPC;

/**
 * Created by bholmes on 29/11/2014.
 */
public class AttackAction implements Action {

    private NPC npc;

    public AttackAction(NPC npc) {
        this.npc = npc;
    }

    public void execute() {
        // TODO: Switch screens to
    }

    public NPC getNpc() {
        return npc;
    }

    public String getLabel() {
        return "Attack "
                + npc.getNpcType().getName()
                + " (lvl "
                + npc.getLevel()
                + ")";
    }
}
