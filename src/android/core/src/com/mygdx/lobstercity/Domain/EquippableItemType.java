package com.mygdx.lobstercity.Domain;

import java.util.List;

/**
 * Created by bholmes on 10/07/2014.
 */
public class EquippableItemType {
    private final int id;
    private final List<EquipmentSlots> slots;

    public EquippableItemType(int id, List<EquipmentSlots> slots) {
        this.id = id;
        this.slots = slots;
    }

    public int getId() {
        return id;
    }

    public List<EquipmentSlots> getSlots() {
        return slots;
    }
}
