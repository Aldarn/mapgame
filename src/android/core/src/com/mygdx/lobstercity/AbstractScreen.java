package com.mygdx.lobstercity;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by bholmes on 02/07/2014.
 */
public abstract class AbstractScreen implements Screen {
    public Actor createUi() {
        return null;
    }

    public abstract ScreenType getBackScreen();
}
