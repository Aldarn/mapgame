package com.mygdx.lobstercity.Interfaces;

import com.mygdx.lobstercity.Domain.Player;
import com.mygdx.lobstercity.Domain.TileType;

import java.util.List;

/**
 * Created by bholmes on 13/07/2014.
 */
public interface WantsTileTypes {
    public void onTileTypesReceived(List<TileType> tileTypes);

    public void onTileTypesFailed();
}
