package com.mygdx.lobstercity.Interfaces;

import com.mygdx.lobstercity.Domain.Player;

/**
 * Created by bholmes on 30/06/2014.
 */
public interface WantsPlayer {
    public void onPlayerReceived(Player player);

    public void onPlayerFailed();
}
