package com.mygdx.lobstercity.Interfaces;

import com.mygdx.lobstercity.Domain.AttackableNPCType;
import com.mygdx.lobstercity.Domain.NPC;
import com.mygdx.lobstercity.Domain.NPCType;
import com.mygdx.lobstercity.Domain.TileType;

import java.util.List;
import java.util.Map;

/**
 * Created by bholmes on 23/11/2014.
 */
public interface WantsMapNPCs {
    public void onMapNpcsReceived(List<NPC> npcs);

    public void onMapNpcsReceived(Map<Integer, NPCType> npcTypes, Map<Integer,
            AttackableNPCType> attackableNPCTypes, List<NPC> npcs);

    public void onMapNpcsFailed();
}
