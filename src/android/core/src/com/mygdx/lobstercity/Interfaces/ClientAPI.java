package com.mygdx.lobstercity.Interfaces;

/**
 * Created by bholmes on 30/06/2014.
 */
public interface ClientAPI {
    public void getPlayer(int id, WantsPlayer responseHandler);

    public void getMap(int id, WantsMap responseHandler);

    public void getTileTypes(WantsTileTypes responseHandler);

    public void getMapNPCs(int id, int playerId, boolean types, WantsMapNPCs responseHandler);
}
