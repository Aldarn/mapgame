package com.mygdx.lobstercity.Interfaces;

import com.mygdx.lobstercity.Domain.GameMap;

/**
 * Created by bholmes on 30/06/2014.
 */
public interface WantsMap {
    public void onMapReceived(GameMap gameMap);

    public void onMapFailed();
}
