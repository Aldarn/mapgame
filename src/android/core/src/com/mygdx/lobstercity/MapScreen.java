package com.mygdx.lobstercity;

/**
 * Created by bholmes on 02/07/2014.
 */
public class MapScreen extends GameScreen {

    public MapScreen(Game game) {
        super(game);
    }

    public ScreenType getBackScreen() {
        return ScreenType.MENU;
    }

    public void gameScreenHUDRender() {
        // TODO: Render HUD
    }

    public void gameScreenRender(float delta) {
        // TODO: Render map
    }
}
