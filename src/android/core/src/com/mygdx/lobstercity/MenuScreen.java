package com.mygdx.lobstercity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.lobstercity.Domain.Actions.Action;
import com.mygdx.lobstercity.Domain.GameMap;
import com.mygdx.lobstercity.Domain.LobsterObject;
import com.mygdx.lobstercity.Domain.LobsterTiledMapRenderer;
import com.mygdx.lobstercity.Domain.NPC;
import com.mygdx.lobstercity.Domain.Player;
import com.mygdx.lobstercity.Domain.SpriteAnimator;
import com.mygdx.lobstercity.Domain.SpriteState;
import com.mygdx.lobstercity.Utils.Loader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by bholmes on 02/07/2014.
 */
public class MenuScreen extends GameScreen {
    Loader loader;
    boolean loaded = false;

    private TextureAtlas atlas;
    private TiledMap map;
    private LobsterTiledMapRenderer renderer;
    private OrthographicCamera camera;
//    private OrthoCamController cameraController;
    private BitmapFont font;
    private SpriteBatch batch;

    public static int TILE_SIZE = 32;
    private static int OFFSET_TOP = 33;
    private static int OFFSET_BOTTOM = 27;

    private LobsterObject playerObject;

    private Table arrows;
    private Table actionPanel;

    public MenuScreen(Game game) {
        super(game);
        setupCamera();

        font = new BitmapFont();
        batch = new SpriteBatch();
        loader = new Loader();
        loader.setPercentageLoaded((game.isLoaded()) ? 100 : 70);
    }

    private void setupCamera() {
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        // Set the camera to show 10 tiles vertically and a proportionate amount horizontally
        camera = new OrthographicCamera();
        // camera.setToOrtho(false, (w / (h - (OFFSET_TOP + OFFSET_BOTTOM))) * 10, 10);
        camera.setToOrtho(false, (w / (h - 60)) * 10, 10);
        camera.update();
    }

    public ScreenType getBackScreen() {
        return ScreenType.EXIT;
    }

    private void load() {
        createMapRenderer(loadNpcs());
    }

    private List<LobsterObject> loadNpcs() {
        List<LobsterObject> npcs = new ArrayList<LobsterObject>();

        // Only load the player if it hasn't already been loaded
        if(playerObject == null) {
            playerObject = new LobsterObject(Game.PLAYER, camera);
        }
        npcs.add(playerObject);

        // Load npc's
        for(NPC npc : Game.MAP_NPCS) {
            npcs.add(new LobsterObject(npc, camera));
        }

        return npcs;
    }

    private void createMapRenderer(List<LobsterObject> npcs) {
        // cameraController = new OrthoCamController(camera);
        // Gdx.input.setInputProcessor(cameraController);

        {
            // Get the tile images from the texture
            atlas = new TextureAtlas(Gdx.files.internal("tiles/textures/TileTexture.atlas"));

            // Create the map object
            map = new TiledMap();

            // Create a list of layers
            MapLayers layers = map.getLayers();

            // Create the bottom layer
            TiledMapTileLayer layer = new TiledMapTileLayer(
                    Game.MAP.getWidth(),
                    Game.MAP.getHeight(),
                    MenuScreen.TILE_SIZE,
                    MenuScreen.TILE_SIZE
            );

            // Create the tiles
            for (int y = 0; y < Game.MAP.getHeight(); y++) {
                for (int x = 0; x < Game.MAP.getWidth(); x++) {
                    TiledMapTileLayer.Cell cell = new TiledMapTileLayer.Cell();
                    StaticTiledMapTile tile = Game.TILE_TYPES.get(Game.MAP.getTileTypeId(x, y)).getTile(atlas);
                    cell.setTile(tile);
                    layer.setCell(x, (Game.MAP.getHeight() - 1) - y, cell);
                }
            }

            // Add the layer
            layers.add(layer);

            // Add an empty layer for npc's
            TiledMapTileLayer npcLayer = new TiledMapTileLayer(
                    Game.MAP.getWidth(),
                    Game.MAP.getHeight(),
                    MenuScreen.TILE_SIZE,
                    MenuScreen.TILE_SIZE
            );
            layers.add(npcLayer);
        }

        renderer = new LobsterTiledMapRenderer(map, 1 / 32f, npcs);
        loaded = true;
        loader.setPercentageLoaded(100);
    }

    public void gameScreenRender(float delta) {
        // TODO: Render menu

        if(loaded) {
            renderMap(delta);
            setCamera();
        } else if(game.isLoaded()) {
            load();
        } else {
            renderLoader();
        }

        renderFPS();
        // renderActionPanel();
    }

    private void renderLoader() {
        batch.begin();
        batch.draw(
            loader.getCurrentFrame(),
            (Gdx.graphics.getWidth() / 2) - (loader.getCurrentFrame().getWidth() / 2),
            (Gdx.graphics.getHeight() / 2) - (loader.getCurrentFrame().getHeight() / 2)
        );
        batch.end();
    }

    private void renderFPS() {
        batch.begin();
        font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, 20);
        batch.end();
    }

    private void renderMap(float delta) {
        // Prepare for embedded map drawing by applying the desired viewport for the map
        Gdx.gl.glViewport(0, OFFSET_BOTTOM, Gdx.graphics.getWidth(),  Gdx.graphics.getHeight() - (OFFSET_TOP + OFFSET_BOTTOM));
        camera.update();
        renderer.setView(camera);
        renderer.render(camera, delta);

//        batch.begin();
//        font.draw(batch, "zoom: " + camera.zoom + "; worldSizeX: " + worldSizeX + "; wordSizeY: " + worldSizeY +
//                "; minCameraX: " + minCameraX + "; minCameraY: " + minCameraY + "; maxCameraX: "
//                + maxCameraX + "; maxCameraY: " + maxCameraY + "; x: " + (camera.position.x * 32) + "; y: "
//                + (camera.position.y * 32), 10, 40);
//        batch.end();
    }

//    public class OrthoCamController extends InputAdapter {
//        final OrthographicCamera camera;
//        final Vector3 curr = new Vector3();
//        final Vector3 last = new Vector3(-1, -1, -1);
//        final Vector3 delta = new Vector3();
//
//        public OrthoCamController (OrthographicCamera camera) {
//            this.camera = camera;
//        }
//
//        @Override
//        public boolean touchDragged (int x, int y, int pointer) {
//            camera.unproject(curr.set(x, y, 0));
//            if (!(last.x == -1 && last.y == -1 && last.z == -1)) {
//                camera.unproject(delta.set(last.x, last.y, 0));
//                delta.sub(curr);
//                camera.position.add(delta.x, delta.y, 0);
//
//                camera.position.x = MathUtils.clamp(camera.position.x, camera.viewportWidth / 2f, Game.MAP.getWidth() - camera.viewportWidth / 2f);
//                camera.position.y = MathUtils.clamp(camera.position.y, camera.viewportHeight / 2f, Game.MAP.getHeight() - camera.viewportHeight / 2f);
//            }
//            last.set(x, y, 0);
//            return false;
//        }
//
//        @Override
//        public boolean touchUp (int x, int y, int pointer, int button) {
//            last.set(-1, -1, -1);
//            return false;
//        }
//    }

    /**
     *
     * HUD Methods
     *
     */

    public Actor createUi() {
        // TODO: Menu UI
        return new Label("", new Skin(Gdx.files.internal("skins/uiskin.json")));
    }

    private void createActionPanel() {
        actionPanel = new Table(defaultSkin);
        actionPanel.padLeft(20.0f);
        actionPanel.padRight(20.0f);
        actionPanel.padTop(20.0f);
        actionPanel.padBottom(20.0f);

        actionPanel.setX(Gdx.graphics.getWidth() - 50);
        actionPanel.setY(Gdx.graphics.getHeight() - 50);

        actionPanel.setDebug(true);

        stage.addActor(actionPanel);
        ActionService.PANEL = actionPanel;
    }

    private void renderActionPanel() {
//        actionPanel.clear();
//
//        for(List<Action> actions : ActionService.ACTIONS.values()) {
//            for(Action action : actions) {
//                actionPanel.row();
//                actionPanel.add(new Label(action.getLabel(), new Skin(Gdx.files.internal("skins/uiskin.json")))).fillX();
//            }
//        }
    }

    public void gameScreenHUDRender() {
        this.table.row();
        this.table.add(createUi()).expand();
        this.table.row();
        this.table.add(createArrows().align(Align.left)).align(Align.left).fillX();

        createActionPanel();
    }

    private enum Arrows {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    private Table createArrows() {
        arrows = new Table(defaultSkin);
        arrows.padLeft(10.0f);
        arrows.padBottom(10.0f);
        arrows.getColor().a = 0.25f;

        HorizontalGroup upArrowContainer = new HorizontalGroup();
        Image upArrowImage = new Image(new Texture(Gdx.files.internal("buttons/upArrow.png")));
        // Button upArrow = new Button(upArrowImage, defaultSkin);
        upArrowImage.setName(Arrows.UP.name());
        upArrowImage.addListener(getMoveHandler());
        upArrowContainer.addActor(upArrowImage);
        arrows.add(upArrowContainer);
        arrows.row();

        HorizontalGroup sideArrowsContainer = new HorizontalGroup();
        Image leftArrowImage = new Image(new Texture(Gdx.files.internal("buttons/leftArrow.png")));
        // Button leftArrow = new Button(leftArrowImage, defaultSkin);
        leftArrowImage.setName(Arrows.LEFT.name());
        leftArrowImage.addListener(getMoveHandler());
        sideArrowsContainer.addActor(leftArrowImage);
        sideArrowsContainer.space(32.0f);

        Image rightArrowImage = new Image(new Texture(Gdx.files.internal("buttons/rightArrow.png")));
        // Button rightArrow = new Button(rightArrowImage, defaultSkin);
        rightArrowImage.setName(Arrows.RIGHT.name());
        rightArrowImage.addListener(getMoveHandler());
        sideArrowsContainer.addActor(rightArrowImage);
        arrows.add(sideArrowsContainer);
        arrows.row();

        HorizontalGroup downArrowContainer = new HorizontalGroup();
        Image downArrowImage = new Image(new Texture(Gdx.files.internal("buttons/downArrow.png")));
        // Button downArrow = new Button(downArrowImage, defaultSkin);
        downArrowImage.setName(Arrows.DOWN.name());
        downArrowImage.addListener(getMoveHandler());
        downArrowContainer.addActor(downArrowImage);
        arrows.add(downArrowContainer);

        return arrows;
    }

    /**
     *
     * Event Handlers
     *
     */

    private InputListener getMoveHandler() {
        return new InputListener() {
            @Override
            public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor)
            {
                System.out.println("touchdown");
                movePlayer(event, event.getListenerActor());
            }
            @Override
            public void exit (InputEvent event, float x, float y, int pointer, Actor toActor)
            {
                System.out.println("touchup");
                playerObject.stop();
            }
        };
    }

    private void movePlayer(InputEvent event, Actor actor) {
        System.out.println("Arrow pressed: " + actor.getName());

        switch(Arrows.valueOf(actor.getName())) {
            case UP:
                playerObject.movedUp();
                break;
            case DOWN:
                playerObject.movedDown();
                break;
            case LEFT:
                playerObject.movedLeft();
                break;
            case RIGHT:
                playerObject.movedRight();
                break;
        }
    }

    private void setCamera() {
        camera.position.x = MathUtils.clamp((float)playerObject.getCurrentMapX(), camera.viewportWidth / 2f, Game.MAP.getWidth() - camera.viewportWidth / 2f);
        camera.position.y = MathUtils.clamp((float)playerObject.getCurrentMapY(), camera.viewportHeight / 2f, Game.MAP.getHeight() - camera.viewportHeight / 2f);
    }
}