package com.mygdx.lobstercity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.lobstercity.Domain.Player;

/**
 * Created by bholmes on 02/07/2014.
 */
public abstract class GameScreen extends AbstractScreen {

    protected Game game;
    protected Stage stage;

    protected Skin defaultSkin;

    protected Table table;
    protected HorizontalGroup playerInfoBar;
    protected HorizontalGroup quickLinksBar;

    public GameScreen(Game game) {
        this.game = game;

        setupStage();
    }

    private void setupStage() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        defaultSkin = new Skin(Gdx.files.internal("skins/uiskin.json"));

        table = new Table(defaultSkin);
        table.add(createPlayerInfoBar()).align(Align.left).fillX();
        gameScreenHUDRender();
        table.row();
        table.add(createQuickLinksBar());

        table.setFillParent(true);
//        table.setDebug(true);

        stage.addActor(table);
    }

    private HorizontalGroup createQuickLinksBar() {
        quickLinksBar = new HorizontalGroup();
        Label fake = new Label("*** Quick Links ***", defaultSkin);
        quickLinksBar.addActor(fake);

        return this.quickLinksBar;
    }

    private HorizontalGroup createPlayerInfoBar() {
        playerInfoBar = new HorizontalGroup().space(5);
        addStat(playerInfoBar, "icons/level_32x32.png", "N/A");
        addStat(playerInfoBar, "icons/gold_32x32.png","N/A");
        addStat(playerInfoBar, "icons/diamond_32x32.png", "N/A");
        addStat(playerInfoBar, "icons/experience_32x32.png", "N/A");

        return this.playerInfoBar;
    }

    private void addStat(Group panel, String iconPath, String value) {
        Image icon = new Image(new Texture(Gdx.files.internal(iconPath)));
        Label valueLabel = new Label(value, new Skin(Gdx.files.internal("skins/uiskin.json")));
        panel.addActor(icon);
        panel.addActor(valueLabel);
    }

    public void updateStats() {
        if(Game.PLAYER != null) {
            ((Label)playerInfoBar.getChildren().get(1)).setText(String.valueOf(Game.PLAYER.getLevel()));
            ((Label)playerInfoBar.getChildren().get(3)).setText(String.valueOf(Game.PLAYER.getGold()));
            ((Label)playerInfoBar.getChildren().get(5)).setText(String.valueOf(Game.PLAYER.getDiamonds()));
            ((Label)playerInfoBar.getChildren().get(7)).setText(String.valueOf(Game.PLAYER.getExperience()));
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.125f, 0.125f, 0.125f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw the game screen
        gameScreenRender(delta);

        // Ensure the stats are set correctly
        updateStats();

        // Prepare for stage drawing by updating the viewport
        stage.getViewport().apply();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    protected abstract void gameScreenHUDRender();

    protected abstract void gameScreenRender(float delta);

    @Override
    public void dispose() {
        this.stage.dispose();
    }

    @Override
    public void show() {
        // called when this screen is set as the screen with game.setScreen();
    }

    @Override
    public void hide() {
        // called when current screen changes from this to a different screen
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void resize(int width, int height) {
        // Resize the ui, true means the camera is centered
        stage.getViewport().update(width, height, true);
        table.setFillParent(true);
        table.invalidate();
    }
}
