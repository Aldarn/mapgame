package com.mygdx.lobstercity;

/**
 * Created by bholmes on 02/07/2014.
 */
public enum ScreenType {
    MENU("Menu"),
    MAP("Map"),
    EXIT("Exit");

    private String name;

    private ScreenType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
