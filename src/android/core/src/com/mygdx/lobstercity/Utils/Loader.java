package com.mygdx.lobstercity.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by bholmes on 02/07/2014.
 */
public class Loader {
    private Texture[] loadingImages = {
            new Texture("loader/loader0000.png"),
            new Texture("loader/loader0001.png"),
            new Texture("loader/loader0002.png"),
            new Texture("loader/loader0003.png"),
            new Texture("loader/loader0004.png"),
            new Texture("loader/loader0005.png"),
            new Texture("loader/loader0006.png"),
            new Texture("loader/loader0007.png"),
            new Texture("loader/loader0008.png"),
            new Texture("loader/loader0009.png"),
    };

    private int percentageLoaded;
    private Label loadingLabel = new Label("LOADING", new Skin(Gdx.files.internal("skins/uiskin.json")));

    public Loader() {
        this.percentageLoaded = 0;
    }

    public int getPercentageLoaded() {
        return this.percentageLoaded;
    }

    public void setPercentageLoaded(int percent) {
        this.percentageLoaded = percent;
        this.loadingLabel.setText("LOADING " + percent + "%");
    }

    public Texture getCurrentFrame() {
        return loadingImages[(int)Math.floor(this.percentageLoaded / 10)];
    }

    public void draw() {
        // TODO
    }
}
