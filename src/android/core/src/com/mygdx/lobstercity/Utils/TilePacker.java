package com.mygdx.lobstercity.Utils;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

/**
 * Created by bholmes on 13/07/2014.
 */
public class TilePacker {
    public static String TILE_DIR = "/Users/bholmes/src/mapgame/src/android/android/assets/tiles";
    public static String OUTPUT_DIR = "/Users/bholmes/src/mapgame/src/android/android/assets/tiles/textures";
    public static String FILE_NAME = "TileTexture";

    public static void main (String[] args) throws Exception {
        TexturePacker.Settings settings = new TexturePacker.Settings();
        settings.scale = new float[]{0.5f};
        settings.duplicatePadding = true;
        TexturePacker.process(settings, TILE_DIR, OUTPUT_DIR, FILE_NAME);
    }
}
