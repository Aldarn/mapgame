package com.mygdx.lobstercity;

import com.mygdx.lobstercity.Domain.AttackableNPCType;
import com.mygdx.lobstercity.Domain.GameMap;
import com.mygdx.lobstercity.Domain.NPC;
import com.mygdx.lobstercity.Domain.NPCType;
import com.mygdx.lobstercity.Domain.Player;
import com.mygdx.lobstercity.Domain.TileType;
import com.mygdx.lobstercity.Interfaces.ClientAPI;
import com.mygdx.lobstercity.Interfaces.WantsMap;
import com.mygdx.lobstercity.Interfaces.WantsMapNPCs;
import com.mygdx.lobstercity.Interfaces.WantsPlayer;
import com.mygdx.lobstercity.Interfaces.WantsTileTypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game extends com.badlogic.gdx.Game
        implements WantsPlayer, WantsMap, WantsTileTypes, WantsMapNPCs {

    public static ClientAPI API;
    public static Player PLAYER;
    public static Map<Integer, TileType> TILE_TYPES;
    public static GameMap MAP;
    public static List<NPC> MAP_NPCS;
    public static Map<Integer, NPCType> NPC_TYPES;
    public static Map<Integer, AttackableNPCType> ATTACKABLE_NPC_TYPES;

    private static ScreenService SCREEN_SERVICE;
    private static boolean paused = false;
    private static boolean tileTypesLoaded = false;

    public Game (ClientAPI clientAPI) {
        Game.API = clientAPI;

        loadGame();
    }

    private void loadGame() {
        if(!isLoaded()) {
            Game.API.getPlayer(1, this);
            Game.API.getMap(2, this);
            Game.API.getTileTypes(this);
            Game.API.getMapNPCs(2, 1, true, this);
        }
    }

    /**
     * Create and load all assets here.
     *
     */
	@Override
	public void create() {
        Game.SCREEN_SERVICE = new ScreenService(this);
        setScreen(Game.SCREEN_SERVICE.getCurrentScreen());
	}

    @Override
    public void pause() {
        paused = true;
    }

    /**
     * Called when coming out of a pause state.
     * Show the pause screen.
     *
     */
    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void onPlayerReceived(Player player) {
        System.out.println("Player received.");
        Game.PLAYER = player;
    }

    public boolean hasPlayer() { return Game.PLAYER != null; }

    @Override
    public void onPlayerFailed() {
        System.out.println("Failed to load player.");
    }

    @Override
    public void onMapReceived(GameMap gameMap) {
        System.out.println("Map received.");
        Game.MAP = gameMap;
    }

    public boolean hasMap() {
        return Game.MAP != null;
    }

    @Override
    public void onMapFailed() {
        System.out.println("Failed to load map.");
    }

    @Override
    public void onTileTypesReceived(List<TileType> tileTypes) {
        System.out.println("Tile types received.");
        Game.TILE_TYPES = new HashMap<Integer, TileType>();
        for(TileType tileType : tileTypes) {
            Game.TILE_TYPES.put(tileType.getId(), tileType);
        }

        tileTypesLoaded = true;
    }

    public boolean hasTileTypes() {
        return tileTypesLoaded;
    }

    @Override
    public void onTileTypesFailed() {
        System.out.println("Failed to load tile types.");
    }

    public boolean isLoaded() {
        return hasPlayer() && hasMap() && hasTileTypes() && hasNpcs();
    }

    public boolean isPaused() {
        return paused;
    }

    @Override
    public void onMapNpcsReceived(List<NPC> npcs) {
        Game.MAP_NPCS = npcs;
    }

    @Override
    public void onMapNpcsReceived(Map<Integer, NPCType> npcTypes,
            Map<Integer, AttackableNPCType> attackableNPCTypes, List<NPC> npcs) {
        System.out.println("NPCs received.");
        Game.NPC_TYPES = npcTypes;
        Game.ATTACKABLE_NPC_TYPES = attackableNPCTypes;

        onMapNpcsReceived(npcs);
    }

    public boolean hasNpcs() {
        return Game.MAP_NPCS != null && Game.NPC_TYPES != null && Game.ATTACKABLE_NPC_TYPES != null;
    }

    @Override
    public void onMapNpcsFailed() {
        System.out.println("Failed to load map npcs.");
    }
}
