# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: bdholmes.com (MySQL 5.5.41-0ubuntu0.14.04.1)
# Database: lobster
# Generation Time: 2015-04-27 21:39:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table attackable_npc_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attackable_npc_types`;

CREATE TABLE `attackable_npc_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `respawn_delay` int(11) unsigned NOT NULL DEFAULT '0',
  `health` int(11) unsigned NOT NULL DEFAULT '0',
  `damage` int(11) unsigned NOT NULL DEFAULT '0',
  `defence` int(11) unsigned NOT NULL DEFAULT '0',
  `accuracy` int(11) unsigned NOT NULL DEFAULT '0',
  `agility` int(11) unsigned NOT NULL DEFAULT '0',
  `critical` int(11) unsigned NOT NULL DEFAULT '0',
  `block` int(11) unsigned NOT NULL DEFAULT '0',
  `poison` int(11) unsigned NOT NULL DEFAULT '0',
  `life_steal` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `attackable_npc_types` WRITE;
/*!40000 ALTER TABLE `attackable_npc_types` DISABLE KEYS */;

INSERT INTO `attackable_npc_types` (`id`, `respawn_delay`, `health`, `damage`, `defence`, `accuracy`, `agility`, `critical`, `block`, `poison`, `life_steal`)
VALUES
	(1,0,10,1,1,0,0,0,0,0,0),
	(2,0,15,5,5,0,0,0,0,0,0);

/*!40000 ALTER TABLE `attackable_npc_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attackable_npcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attackable_npcs`;

CREATE TABLE `attackable_npcs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `npc_id` int(11) unsigned NOT NULL,
  `attackable_type_id` int(11) unsigned NOT NULL,
  `player_id` int(11) unsigned NOT NULL,
  `respawn_time` int(11) NOT NULL DEFAULT '0',
  `health` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `attackable_type_id` (`attackable_type_id`),
  KEY `npc_id` (`npc_id`),
  KEY `player_id` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `attackable_npcs` WRITE;
/*!40000 ALTER TABLE `attackable_npcs` DISABLE KEYS */;

INSERT INTO `attackable_npcs` (`id`, `npc_id`, `attackable_type_id`, `player_id`, `respawn_time`, `health`)
VALUES
	(1,1,1,1,0,10),
	(2,2,2,1,0,15),
	(3,3,3,1,0,50),
	(4,4,1,1,0,15);

/*!40000 ALTER TABLE `attackable_npcs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table map_areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `map_areas`;

CREATE TABLE `map_areas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `secret` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table map_borders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `map_borders`;

CREATE TABLE `map_borders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `north_area_id` int(11) unsigned NOT NULL,
  `south_area_id` int(11) unsigned NOT NULL,
  `east_area_id` int(11) unsigned NOT NULL,
  `west_area_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table map_doors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `map_doors`;

CREATE TABLE `map_doors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `target_map_id` int(11) unsigned NOT NULL,
  `target_map_x` int(11) NOT NULL DEFAULT '0',
  `target_map_y` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target_map_id` (`target_map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `map_doors` WRITE;
/*!40000 ALTER TABLE `map_doors` DISABLE KEYS */;

INSERT INTO `map_doors` (`id`, `target_map_id`, `target_map_x`, `target_map_y`)
VALUES
	(1,1,1,1);

/*!40000 ALTER TABLE `map_doors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table npc_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `npc_types`;

CREATE TABLE `npc_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `width` int(11) unsigned NOT NULL DEFAULT '10',
  `height` int(11) unsigned NOT NULL DEFAULT '10',
  `walkable` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `sprite` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `npc_types` WRITE;
/*!40000 ALTER TABLE `npc_types` DISABLE KEYS */;

INSERT INTO `npc_types` (`id`, `name`, `width`, `height`, `walkable`, `image`, `sprite`)
VALUES
	(1,'Drifter',0,0,0,NULL,NULL),
	(2,'Bozo',0,0,0,NULL,NULL),
	(3,'Boris',0,0,0,NULL,NULL);

/*!40000 ALTER TABLE `npc_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table npcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `npcs`;

CREATE TABLE `npcs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `npc_type_id` int(11) unsigned NOT NULL,
  `map_id` int(11) unsigned NOT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `level` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `map_id` (`map_id`),
  KEY `npc_type_id` (`npc_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `npcs` WRITE;
/*!40000 ALTER TABLE `npcs` DISABLE KEYS */;

INSERT INTO `npcs` (`id`, `npc_type_id`, `map_id`, `x`, `y`, `level`)
VALUES
	(1,1,1,5,5,1),
	(2,2,1,6,6,2),
	(3,3,1,7,7,5),
	(4,1,1,8,8,2);

/*!40000 ALTER TABLE `npcs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table players
# ------------------------------------------------------------

DROP TABLE IF EXISTS `players`;

CREATE TABLE `players` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `experience` int(11) DEFAULT NULL,
  `stamina` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;

INSERT INTO `players` (`id`, `user_id`, `level`, `experience`, `stamina`)
VALUES
	(1,1,1,0,100);

/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tile_type_hit_rects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tile_type_hit_rects`;

CREATE TABLE `tile_type_hit_rects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tile_type_id` int(11) unsigned NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tile_type_id` (`tile_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tile_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tile_types`;

CREATE TABLE `tile_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `image` varchar(100) NOT NULL DEFAULT '',
  `walkable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `email`)
VALUES
	(1,'Aldarn','fake','ben@bdholmes.com');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table visited_areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `visited_areas`;

CREATE TABLE `visited_areas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` int(11) unsigned NOT NULL,
  `area_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `player_id` (`player_id`),
  KEY `area_id` (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
