# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: bdholmes.com (MySQL 5.5.37-0ubuntu0.12.04.1)
# Database: bdholmes
# Generation Time: 2014-06-16 21:48:02 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table mapgame_attackable_npc_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_attackable_npc_types`;

CREATE TABLE `mapgame_attackable_npc_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `npc_type_id` int(11) unsigned NOT NULL,
  `respawn_delay` int(11) unsigned NOT NULL DEFAULT '0',
  `health` int(11) unsigned NOT NULL DEFAULT '0',
  `strength` int(11) unsigned NOT NULL DEFAULT '0',
  `defence` int(11) unsigned NOT NULL DEFAULT '0',
  `accuracy` int(11) unsigned NOT NULL DEFAULT '0',
  `agility` int(11) unsigned NOT NULL DEFAULT '0',
  `critical` int(11) unsigned NOT NULL DEFAULT '0',
  `block` int(11) unsigned NOT NULL DEFAULT '0',
  `poison` int(11) unsigned NOT NULL DEFAULT '0',
  `life_steal` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `npc_type_id` (`npc_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mapgame_attackable_npc_types` WRITE;
/*!40000 ALTER TABLE `mapgame_attackable_npc_types` DISABLE KEYS */;

INSERT INTO `mapgame_attackable_npc_types` (`id`, `npc_type_id`, `respawn_delay`, `health`, `strength`, `defence`, `accuracy`, `agility`, `critical`, `block`, `poison`, `life_steal`)
VALUES
	(1,1,0,10,1,1,0,0,0,0,0,0),
	(2,1,0,15,5,5,0,0,0,0,0,0);

/*!40000 ALTER TABLE `mapgame_attackable_npc_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mapgame_attackable_npcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_attackable_npcs`;

CREATE TABLE `mapgame_attackable_npcs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `npc_id` int(11) unsigned NOT NULL,
  `attackable_type_id` int(11) unsigned NOT NULL,
  `respawn_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attackable_type_id` (`attackable_type_id`),
  KEY `npc_id` (`npc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mapgame_attackable_npcs` WRITE;
/*!40000 ALTER TABLE `mapgame_attackable_npcs` DISABLE KEYS */;

INSERT INTO `mapgame_attackable_npcs` (`id`, `npc_id`, `attackable_type_id`, `respawn_time`)
VALUES
	(1,1,1,0);

/*!40000 ALTER TABLE `mapgame_attackable_npcs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mapgame_map_areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_map_areas`;

CREATE TABLE `mapgame_map_areas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `secret` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table mapgame_map_borders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_map_borders`;

CREATE TABLE `mapgame_map_borders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `north_area_id` int(11) unsigned NOT NULL,
  `south_area_id` int(11) unsigned NOT NULL,
  `east_area_id` int(11) unsigned NOT NULL,
  `west_area_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table mapgame_map_doors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_map_doors`;

CREATE TABLE `mapgame_map_doors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `target_map_id` int(11) unsigned NOT NULL,
  `target_map_x` int(11) NOT NULL DEFAULT '0',
  `target_map_y` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target_map_id` (`target_map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mapgame_map_doors` WRITE;
/*!40000 ALTER TABLE `mapgame_map_doors` DISABLE KEYS */;

INSERT INTO `mapgame_map_doors` (`id`, `target_map_id`, `target_map_x`, `target_map_y`)
VALUES
	(1,1,1,1);

/*!40000 ALTER TABLE `mapgame_map_doors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mapgame_npc_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_npc_types`;

CREATE TABLE `mapgame_npc_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sprite` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mapgame_npc_types` WRITE;
/*!40000 ALTER TABLE `mapgame_npc_types` DISABLE KEYS */;

INSERT INTO `mapgame_npc_types` (`id`, `name`, `image`, `sprite`)
VALUES
	(1,'Drifter',NULL,NULL);

/*!40000 ALTER TABLE `mapgame_npc_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mapgame_npcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_npcs`;

CREATE TABLE `mapgame_npcs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `map_id` int(11) unsigned NOT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `map_id` (`map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mapgame_npcs` WRITE;
/*!40000 ALTER TABLE `mapgame_npcs` DISABLE KEYS */;

INSERT INTO `mapgame_npcs` (`id`, `map_id`, `x`, `y`)
VALUES
	(1,1,2,2);

/*!40000 ALTER TABLE `mapgame_npcs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mapgame_players
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_players`;

CREATE TABLE `mapgame_players` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `experience` int(11) DEFAULT NULL,
  `stamina` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table mapgame_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_users`;

CREATE TABLE `mapgame_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mapgame_users` WRITE;
/*!40000 ALTER TABLE `mapgame_users` DISABLE KEYS */;

INSERT INTO `mapgame_users` (`id`, `username`, `password`, `email`)
VALUES
	(1,'Aldarn','fake','ben@bdholmes.com');

/*!40000 ALTER TABLE `mapgame_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mapgame_visited_areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mapgame_visited_areas`;

CREATE TABLE `mapgame_visited_areas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` int(11) unsigned NOT NULL,
  `area_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `player_id` (`player_id`),
  KEY `area_id` (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
