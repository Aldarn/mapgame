from PIL import Image, ImageOps
import os
from optparse import OptionParser

def convertImage(sizes, sourcePath, destinationPath):
	for size in sizes:
		destination = destinationPath + "_" + str(size[0]) + "x" + str(size[1]) + ".png"
		try:
			image = Image.open(sourcePath)
			image.thumbnail(size, Image.ANTIALIAS)

			background = Image.new('RGBA', size, (255, 255, 255, 0))
			background.paste(image, ((size[0] - image.size[0]) / 2, (size[1] - image.size[1]) / 2))

			background.save(destination, "PNG")
			print "Saved image " + destination + "..."
		except IOError, e:
			print "Failed to save " + destination + ": " + str(e)

parser = OptionParser()
parser.add_option("-d", "--dir", dest="directory", help="Directory of images to convert location.", metavar="DIR")
parser.add_option("-f", "--file", dest="filePath", help="Image to convert location.", metavar="FILE")
parser.add_option("-i", "--item", dest="item", action="store_true", default=False, help="Item style conversion.")
parser.add_option("-n", "--npc", dest="npc", action="store_true", default=False, help="NPC style conversion.")
parser.add_option("-t", "--tile", dest="tile", action="store_true", default=False, help="Tile style conversion.")
parser.add_option("-p", "--newPath", dest="newPath", help="Path of the output file.", metavar="PATH")
parser.add_option("-w", "--width", dest="width", type=int, help="Width to convert to.", metavar="WIDTH")
parser.add_option("-l", "--height", dest="height", type=int, help="Height to convert to.", metavar="HEIGHT")

args = parser.parse_args()[0]

# Bail if no file path or dir
if args.filePath is None and args.directory is None:
	print "Must supply the path to a file or directory containing images to convert!"
	exit()

# Data for different types
IMAGE_TYPES = [".png", ".jpg", ".gif", ".jpeg"]

ITEMS_PATH = "../src/server/mapgame/images/items"
ITEM_SIZES = [(256, 256), (128, 128), (64, 64)]

NPCS_PATH = "../src/server/mapgame/images/npcs"
NPC_SIZES = [(256, 256), (32, 32)]

TILES_PATH = "../src/server/mapgame/images/tiles"
TILE_SIZES = [(32, 32)]

# Figure out the destination directory based on the type
destinationDir = ""
sizes = [(128, 128)]
if args.item:
	destinationDir = ITEMS_PATH
	sizes = ITEM_SIZES
elif args.npc:
	destinationDir = NPCS_PATH
	sizes = NPC_SIZES
elif args.tile:
	destinationDir = TILES_PATH
	sizes = TILE_SIZES

# Use the new size arguments if specified
if args.width is not None and args.height is not None:
	sizes = [(width, height)]

if args.filePath:
	# Use the new path argument if specified
	if args.newPath is not None:
		destinationName = args.newPath
	else:
		destinationName = os.path.splitext(os.path.basename(args.filePath))[0]

	# Combine the name
	destinationPath = os.path.join(destinationDir, destinationName)

	# Convert the images
	convertImage(sizes, args.filePath, destinationPath)

else:
	fileNames = [fileName for fileName in os.listdir(args.directory) if os.path.isfile(os.path.join(args.directory, fileName)) and os.path.splitext(os.path.basename(fileName))[1].lower() in IMAGE_TYPES]
	for fileName in fileNames:
		convertImage(sizes, os.path.join(args.directory, fileName), os.path.join(destinationDir, os.path.splitext(fileName)[0]))

print "Done!"