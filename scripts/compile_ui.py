import subprocess
import os

BASE_COMPILE_FROM_DIR = "../ui"
BASE_COMPILE_TO_DIR = "../src/map_editor/ui"

fileNames = [fileName for fileName in os.listdir(BASE_COMPILE_FROM_DIR) if os.path.isfile(os.path.join(BASE_COMPILE_FROM_DIR, fileName))]

for fileName in fileNames:
	fromFilePath = os.path.join(BASE_COMPILE_FROM_DIR, fileName)	

	toFilePath = None
	if os.path.splitext(fileName)[1] == ".qrc":
		toFileName = os.path.splitext(fileName)[0] + "_rc.py"
		toFilePath = os.path.join(BASE_COMPILE_TO_DIR, toFileName)
		p = subprocess.Popen(["pyrcc", "-o", toFilePath, fromFilePath])
	elif os.path.splitext(fileName)[1] == ".ui":
		toFileName = os.path.splitext(fileName)[0] + ".py"
		toFilePath = os.path.join(BASE_COMPILE_TO_DIR, toFileName)
		p = subprocess.Popen(["pyuic5", "-o", toFilePath, fromFilePath])

	if toFilePath:
		print "Compiling " + fromFilePath + " to " + toFilePath + "..."		
		sts = os.waitpid(p.pid, 0)
