About
-----

An Android tile based massively multiplayer online role playing game. 

Based on the original "Leet City" I created and sold around 10 years ago, but now on mobile 
and renamed to "Lobster City". The basic premise involves creating a character, exploring 
the game world (lobster city) and defeating different types of cronies, leading up to discovering 
and destroying the mobster boss known as Lobster. 

Your character gains experience defeating enemies, which allows you to level up a variety of different 
character attributes. You also may find many items and artefacts throughout the land, some are randomly 
generated (and possibly very strong), others predefined after beating bosses. 

Being a multiplayer game, you may also challenge your fellow gamers in battle, join clans and take 
down mobster bosses as groups (some bosses require multiple players), sell and trade items, 
play mini games and compete for high scores and much more.

Notes on different aspects of the game
--------------------------------------

Check the Wiki for dev info, business proposals, todo's, etc: 

**https://bitbucket.org/Aldarn/mapgame/wiki/Home**

game based on “map game” / “leet city”

levelling / characters / rank system:
have characters that can level up to e.g. level 5
after that use a “hidden” levelling system based on lp (level points) / sp (spree points) / level
can only have e.g. 5% of people at level 10, 10% level 9, etc
the more people you beat in fights the more lp you gain
sp is a multiplier so if you spree you get bonus lp per win
show your rank against other people at your level
if you get over a certain amount of lp then you level up, and someone at the bottom of the next level gets kicked down
need e.g. 12 hour immunity after levelling up to stop people just getting booted down as soon as they level up
possibly have the highest level achievable grow e.g. if the person at the top of level 10 gets to 1k lp they level up, and then if they get to 1.5k lp at lvl 11 then they level up, and this keeps scaling so you need more lp to get to new levels, and the %ages of people at each level shifts accordingly

or, other system for levelling up:
rather than fixed %ages, beating someone makes them lose lp, you gain lp
the level you are is determined by the number of people that can be at each level based on the total number of levels and the total number of people
each player is ranked by score and level is determined by e.g. 10 people can be lvl 20 (current highest level), 15 at lvl 19, 20 at lvl 18, so if you are ranked 30th you would be level 18 (as lvl 20 is rank 1-10, lvl 19 11-25, 18 26-40)
this would be snapshotted every 5 minutes so there’s not always level fluctuations, mid fight weirdness, etc

items:
items would work similarly to original map game, random item drops from NPCs have random stats based on the enemy killed’s level
also have “artefacts” which have constant stats and are dropped by killing bosses and stuff
possible to combine items using in-game currency (gold or whatever)
make it so that you can pick and choose the stats to copy over
have a limit e.g. 1 extra attribute per combination, up to some max depending on the item type
make it possible to merge two of the same stat e.g. 10 attack + 8 attack -> 12 attack (or some calculation)
have a limit on the amount of stat gain possible per combination (e.g. +2 atk, +4 armour, +2hp = +8 total
combination cost is based on current power level of item (calculated by summing the attributes values (some attributes may have a multiplier e.g. crit %age is worth 5x a normal stat like atk))
may have to limit the total amount of upgrades that can be applied to a weapon to stop rich people from getting ridiculous stats (this could be based on the original power level of the item when it dropped)
items could be upgrades themselves e.g. “atk +10 upgrade”, “5% crit chance” that can’t be used as a weapon but can be combined (at a cost)

misc:
possibility to activate “immunity” for e.g. 12 hours, where people can't attack you, but you can’t fight other people during immunity either
can play offline by loading & caching data of surrounding map

quests:
5 enemies in a row
each get harder
can’t change weapons /equipment during quest
each time you win, the quest gains a level, and all enemies in the quest get better (higher level)
death in quest takes you back to the start

	"story mode"
where you kind of go along a set route for the first few battles
then come to forks in the road
and going different ways reveals different types of enemies / loot / etc
and you can't go back once you've chosen the route
can take two roots using platinum
(paths could converge later on (but they wouldn't know that))

collectable virtual item drops
cards that can be collected, filling a set gives some bonus (unique, rare drops)
“orbs of power” fragments, get 5 and use it to upgrade some stat of some item (not unique, rare drops)

fights:
old map game just calculated it all in one go and showed the results
damage had a range so if your character had 10 atk it would do e.g. 8 - 12dmg randomly
can use similar metrics, but have the type of the weapon weight the random factor e.g. sniper rifle is very accurate so you do 10-12dmg more often than 8-10 etc
maybe change the fight to a more pokemon style, but possible without moves? (up for debate)
each round you can do “defensive”, “accurate” or “aggressive” with different modifiers on your overall attack damage and damage taken next round
can use stuff like potions and poison antidotes etc
killing an npc has a chance of an item drop (if it’s equippable it has random stats)
killing an npc drops gold
killing enemy players robs them of a %age of their gold
killing anything gives lp based on the level difference and speed of victory (e.g. if you win in 1 round you get more lp)
killing enemy players makes them lose lp (as discussed under levelling)

map:
tile based
use generic sprites
only see e.g. 3 tile width from your current position
have high level map view but only with general locations and not showing specific paths to get to places
skull and cross bones or other indicates an npc on a tile
walking into an npc gives “attack or run” screen
have a “stamina” stat that goes down the more fights you engage in or lose
can have items that boost this
can have usable items that boost this e.g. potions
areas of the map regenerate this stat e.g. the pub
in these areas the stat isn’t reduced from fights
can attack any other player at any time, or only those nearby? should there be a limit between the level difference e.g. level 100 can’t attack a level 5 player? this needs some thought 
npc’s in cities
different npc’s do different things e.g. weapon combiner, potion shop, etc

clans:
can join a clan at any time
members of clans can participate in raids with multiple members at once
clans hold loot from raids that are won
designated people in the clan have access to this loot and can give it to members of the clan accordingly
maybe better yet do this automatically
rank people based on participations in raids + time since last reward
ability to see on global map overview where allies in clan are located

raids:
join multiple people to fight one enemy at once
when a player finds a raid (usually a boss, could be a group of enemies) they can automatically add people from their clan to fight with them (e.g. max 3 people)
if more people are needed then more will need to come to the raid location on the map
raids usually need a minimum number of people (e.g. 2), changes between raids
some raids are global and respawn at some random interval (e.g. 25-35 mins), first clan to beat it gets the loot

outside the map:
trades can occur at any time
trades should also act as shops i.e. put stuff into your "trading box” and assign a price people can purchase the items at directly
people can select items they want from you (up to a maximum at once e.g. 5) and offer items and or money in return
minigames (will come later)
forums
global message board on front page that you can pay to post to

spells:
you can amass spells (have to use some mana or whatever to use them)
mostly one use type things
examples;
teleport to city x
next 5 min deal more dmg (or other stat)
show particular route on map
more 

currency:
gold is won through fights, quests etc
have additional monetary system e.g. platinum that is much harder to get but can be used to buy stuff gold can’t
you have to pay $ to get platinum, or trade in-game stuff for it (but it’s only every created by someone paying $)

technical:
mysql db to begin with for holding user data etc
rest api written in django to return all data as json
this allows the app to get & update data just by calling the api asynchronously (or synchronously depending on the situation)
can use the same api to create a web interface easily in the future

app:
can slide left / right to reveal multiple “pages” of the game
page for people to post messages (platinum per number of characters)
page for your equipment
page for stats
page for map
page for trades
etc
could have mini row of icons along the bottom to jump to different pages quickly? and notification bubbles on those when stuff changes
have global announcements and stuff pop up as mini messages
level up, gold won, etc
game updates e.g. quests extended and so forth

marketing:
static .info (or other) page indicating platforms to play on
just android to start
can extend to use web too
bonuses for user referrals via e-mails & social networks
5 platinums per person with bonuses at 5, 10 etc
create some cool info graphic that fills up depending on how many people that person has recruited

earning platinums:
leave feedback,5 platinum
refer friend scheme (mentioned above)
more...

testing:
A/B test the quest mode and map game mode as two different apps
Maybe even C with both combined, or merge after and gauge response?