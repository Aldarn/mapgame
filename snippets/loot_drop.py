import re
import math
import random

def getEnemyLevelModifiedValue(value, enemyLevel):
	if "x" in value:
		value = value.replace("x", str(enemyLevel))
	return math.floor(eval(value))

ENEMY_LEVEL = 10
STAT_CAP = lambda enemyLevel: enemyLevel * 4
STAT_MODIFIERS = {
	"strength": {
		"min": getEnemyLevelModifiedValue("(x * 1.25) + 2", ENEMY_LEVEL),
		"max": getEnemyLevelModifiedValue("(x * 1.5) + 8", ENEMY_LEVEL),
		"weightModifier": 1.75
	},
	"critical": {
		"min": 0,
		"max": getEnemyLevelModifiedValue("(x * 0.2) + 5", ENEMY_LEVEL),
		"maxCap": 10,
		"weightModifier": 5
	}	
}

# def getWeightSum(rangeSize):
# 	return rangeSize * (rangeSize + 1) / 2

# def getWeight(index, rangeSize, modifier):
# 	# TODO: How to incorporate modifier?
# 	return (rangeSize - index) 

# def getWeightedStat():
# 	int sum_of_weight = 0;
# 	for(int i=0; i<num_choices; i++) {
# 	   sum_of_weight += choice_weight[i];
# 	}
# 	int rnd = random(sum_of_weight);
# 	for(int i=0; i<num_choices; i++) {
# 	  if(rnd < choice_weight[i])
# 	    return i;
# 	  rnd -= choice_weight[i];
# 	}

# def getStat(min, max, modifier = 1.2, weightLower = True):
# 	print "mod max: " + str(float(((max + 2) - min)))
# 	print "rand(" + str(modifier) + ", " + str(modifier ** float(((max + 2) - min))) + ")"
# 	x = random.uniform(modifier, modifier ** float(((max + 2) - min)))
# 	print "random #: " + str(x)

# 	if weightLower:
# 		return (((max + 2) - min) - math.floor(math.log(x) / math.log(modifier))) + (min - 1)
# 	return math.floor((math.log(x) / math.log(modifier)) + (min - 1))

# def capModifierForRangeDifference(modifier, rangeDifference):
# 	if rangeDifference <= 5 && modifier > 2.3:
# 		modifier = 2.3
# 	elif rangeDifference <= 10 && modifier > 1.6:
# 		modifier = 1.6
# 	elif rangeDifference <= 15 && modifier > 1.4:
# 		modifier = 1.4

"""
You can plot this in wolfram alpha: 

plot floor(min + x ^ modifier * (max - min + 1)) for x in [0, 1]
e.g. http://www.wolframalpha.com/input/?i=plot+floor%285+%2B+x%5E2+*+%2815+-+5+%2B+1%29%29+for+x+in+%5B0%2C+1%5D
"""
def getStat(min, max, modifier):
	offset = max - min + 1
	r = random.random()
	print r
	return math.floor(min + (r ** modifier) * offset)

totalStats = 0
for stat, data in STAT_MODIFIERS.iteritems():
	if "minCap" in data and data["min"] > data["minCap"]:
		data["min"] = data["minCap"]
	if "maxCap" in data and data["max"] > data["maxCap"]:
		data["max"] = data["maxCap"]

	print data["min"], data["max"], data["weightModifier"]
	statValue = getStat(data["min"], data["max"], data["weightModifier"])
	if totalStats + statValue >= STAT_CAP:
		print stat + ": " + str(statValue) + " MAX STATS CAP HIT"
		statValue = STAT_CAP - totalStats
		break
	print stat + ": " + str(statValue)

